import { Event } from '../../shared/models/event';
import { EventService } from '../../shared/services/event.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-single-event',
  templateUrl: './single-event.component.html',
  styleUrls: ['./single-event.component.css']
})
export class SingleEventComponent implements OnInit {

  private id: number;

  event: Event;

  modalReference: any;

  editHeader: boolean = false;

  changeTitle: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private eventService: EventService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      params => {
        this.id = +params.get('id');
        console.log(this.id);

        this.eventService.getEvent(this.id)
          .subscribe(
            res => {
              console.log(res);
              this.event = res;
              if (res['eventDate'])
                this.event.eventDate = new Date(res['eventDate']);
            },
            error => {
              console.log(error);
              if(error.status === 401) {
                this.router.navigate(['login']);
              } else {
                this.router.navigate(['404']);
              }
            }
          );
      }
    );
  }

  updateEvent() {
    console.log("update event");
    
    let body: Event;
    body = this.event;

    body.id = 0;
    console.log(body);

    this.eventService.updateEvent(this.id, body)
      .subscribe(
        res => {
          console.log(res);
          console.log(res['id']);
        },
        error => {
          console.log(error);
        }
      );
  }

  deleteEvent() {
    console.log("add event");
    this.eventService.deleteEvent(this.id)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['events']);
        },
        error => {
          console.log();
        }
      );
  }

  open(content, id) {
    this.modalReference = this.modalService.open(content)
    this.modalReference.result.then((result) => {
      console.log(`Closed with: ${result}`);
    }, (reason) => {
      console.log(`Dismissed ${this.getDismissReason(reason)}`);
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
