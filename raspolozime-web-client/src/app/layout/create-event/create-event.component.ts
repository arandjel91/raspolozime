import { Component, OnInit } from '@angular/core';
import { Event } from '../../shared/models/event';
import { EventService } from '../../shared/services/event.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {

  event: Event = <Event>{};

  constructor(
    private router: Router,
    private eventService: EventService) { }

  ngOnInit() {
  }

  addEvent() {
    console.log("add event");
    let body: Event;
    body = this.event;
    console.log(body);
    
    this.eventService.addEvent(body)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['events', res['id']]);
        },
        error => {
          console.log(error);
        }
      );
  }

}
