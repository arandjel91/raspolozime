import { Event } from '../../shared/models/event';
import { EventService } from '../../shared/services/event.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit, OnDestroy {

  private key: string;
  private sortAsc = false;
  params = {'page': 0, 'size': 6, sort: 'eventDate,DESC'};

  events: Event[];
  eventsTriplets: Event[][];

  isFirstPage;
  isLastPage;
  totalElements;
  totalPages;
  pageNumber;
  pageSize;
  pageSort;
  numberOfElements;

  pageRange;

  sub;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private eventService: EventService) { }

  ngOnInit() {
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.params['page'] = +params['page'] || 0;
        this.params['size'] = +params['size'] || +this.params['size'];
        this.params['sort'] = params['sort'] || this.params['sort'];
        this.getEvents();
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  private getEvents() {
    this.eventService.getAll(this.params)
      .subscribe(
        res => {
          if (res) {
            this.events = res['content'];
            console.log(this.events);

            let tmp: Event[] = [];
            this.events.forEach(function (element) { tmp.push(element); });

            this.eventsTriplets = [];
            while(tmp.length) {
              this.eventsTriplets.push(tmp.splice(0,3));
            }
            
            console.log(this.eventsTriplets);

            this.getPageInfo(res);
            this.setPageRange();
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  onSizeChange(size: number) {
    this.params['page'] = 0;
    this.params['size'] = size;
    if (this.params['size'] > 50)
      this.params['size'] = 50;

    this.router.navigate(['events'], { queryParams: this.params });
  }

  sort(key: string) {
    this.key = key;

    const sortOrder = this.sortAsc ? 'ASC' : 'DESC';

    this.params['sort'] = this.key + ',' + sortOrder;
    this.params['page'] = 0;

    this.router.navigate(['events'], { queryParams: this.params });
    this.sortAsc = !this.sortAsc;
  }

  setPage(page: number) {
    if (page < 0 || page > this.totalPages - 1) {
        return;
    }

    this.pageNumber = page;
    this.params['page'] = page;

    this.router.navigate(['events'], { queryParams: this.params });
  }

  getPageInfo(res) {
    this.isFirstPage = res['first'];
    this.isLastPage = res['last'];
    this.totalElements = res['totalElements'];
    this.totalPages = res['totalPages'];
    this.pageNumber = res['number'];
    this.pageSize = res['size'];
    this.pageSort = res['sort'];
    this.numberOfElements = res['numberOfElements'];
  }

  setPageRange() {
    this.pageRange = [];
    let startPage: number;
    let endPage: number;

    startPage = this.totalPages - this.pageNumber > 10 ? this.pageNumber : 0;
    endPage = this.pageNumber + 10 > this.totalPages ? this.totalPages : this.pageNumber + 10;

    if (this.totalPages <= 10) {
        // less than 10 total pages so show all
        startPage = 0;
        endPage = this.totalPages;
    } else {
        // more than 10 total pages so calculate start and end pages
        if (this.pageNumber <= 5) {
            startPage = 0;
            endPage = 10;
        } else if (this.pageNumber + 4 >= this.totalPages) {
            startPage = this.totalPages - 10;
            endPage = this.totalPages;
        } else {
            startPage = this.pageNumber - 6;
            endPage = this.pageNumber + 4;
        }
    }

    for (; startPage < endPage; startPage++) {
      this.pageRange.push(startPage);
    }
  }

}
