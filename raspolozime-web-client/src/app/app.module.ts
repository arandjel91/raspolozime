import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './layout/home/home.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { ApiService } from './shared/services/api.service';
import { ConfigService } from './shared/services/config.service';
import { NgbDateNativeAdapter } from './shared/services/ngb-date-adapter.service';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from './shared/services/user.service';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { FooterComponent } from './layout/footer/footer.component';
import { GuestGuard } from './shared/guard/guest.guard';
import { LoginComponent } from './login/login.component';
import { AuthService } from './shared/services/auth.service';
import { SignupComponent } from './signup/signup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginGuard } from './shared/guard/login.guard';
import { UserProfileComponent } from './layout/user-profile/user-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { EventComponent } from './layout/event/event.component';
import { EventService } from './shared/services/event.service';
import { SingleEventComponent } from './layout/single-event/single-event.component';
import { CreateEventComponent } from './layout/create-event/create-event.component';
import { DateFormatPipe } from './shared/pipes/date-format.pipe';
import { SummaryPipe } from './shared/pipes/summary.pipe';

export function initUserFactory(userService: UserService) {
    console.log("initUserFactory");
    return () => userService.initUser();
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    SignupComponent,
    UserProfileComponent,
    ChangePasswordComponent,
    EventComponent,
    SingleEventComponent,
    CreateEventComponent,
    DateFormatPipe,
    SummaryPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot()
  ],
  providers: [
    ApiService,
    UserService,
    ConfigService,
    AuthService,
    EventService,
    GuestGuard,
    LoginGuard,
    {
      'provide': NgbDateAdapter, 
      'useClass': NgbDateNativeAdapter
    },
    {
      'provide': APP_INITIALIZER,
      'useFactory': initUserFactory,
      'deps': [UserService],
      'multi': true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
