import { ApiService } from './api.service';
import { ConfigService } from './config.service';
import { Injectable } from '@angular/core';

@Injectable()
export class EventService {

  constructor(
    private apiService: ApiService,
    private config: ConfigService
  ) { }

  getAll(params?: any) {
    return this.apiService.get(this.config.events_url, params);
  }

  getEvent(id: number) {
    return this.apiService.get(this.config.events_url + '/' + id);
  }

  addEvent(body: any) {
    return this.apiService.post(this.config.events_url, body);
  }

  updateEvent(id: number, body: any) {
    return this.apiService.put(this.config.events_url + '/' + id, body);
  }

  deleteEvent(id: number) {
    return this.apiService.delete(this.config.events_url + '/' + id);
  }
}
