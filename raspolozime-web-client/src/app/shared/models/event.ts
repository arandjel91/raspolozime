
export interface Event {

  id: number;
  title: string;
  description: string;
  location: string;
  topEvent: boolean;
  owner: string;
  eventDate: Date;
}
