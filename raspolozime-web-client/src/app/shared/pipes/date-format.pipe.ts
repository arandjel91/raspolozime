import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
    name: 'dateformat',
})
export class DateFormatPipe implements PipeTransform {
    
    transform(value: string) {
        console.log(value);
        if(!value)
            return null;

        var datePipe = new DatePipe("en-US");
        value = datePipe.transform(value, 'dd.MM.yyyy.');
        console.log(value);
        return value;
    }
}