import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
    name: 'summary',
})
export class SummaryPipe implements PipeTransform {
    
    transform(value: string, limit?: number) {
        if(!value)
            return null;

        let actualLimit = (limit) ? limit : 100;

        if (value.length > actualLimit)
            return value.substring(0, actualLimit) + '...';
        
        return value;
    }
}