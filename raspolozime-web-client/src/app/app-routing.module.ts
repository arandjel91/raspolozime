import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { EventComponent } from './layout/event/event.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { GuestGuard } from './shared/guard/guest.guard';
import { LoginGuard } from './shared/guard/login.guard';
import { HomeComponent } from './layout/home/home.component';
import { LoginComponent } from './login/login.component';
import { CreateEventComponent } from './layout/create-event/create-event.component';

import { NotFoundComponent } from './not-found/not-found.component';
import { SignupComponent } from './signup/signup.component';
import { SingleEventComponent } from './layout/single-event/single-event.component';
import { UserProfileComponent } from './layout/user-profile/user-profile.component';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full'
  },
  {
    path: 'signup',
    component: SignupComponent,
    canActivate: [GuestGuard],
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'events/:id',
    component: SingleEventComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'events',
    component: EventComponent
  },
  {
    path: 'create-event',
    component: CreateEventComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'profile',
    component: UserProfileComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'change-password',
    component: ChangePasswordComponent,
    canActivate: [LoginGuard]
  },
  /*
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AdminGuard]
  },
  */
  {
    path: '404',
    component: NotFoundComponent
  },
  /*
  {
    path: '403',
    component: ForbiddenComponent
  },
  */
  {
    path: '**',
    redirectTo: '/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
