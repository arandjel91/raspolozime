package me.raspolozi.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import me.raspolozi.entities.Event;
import me.raspolozi.entities.Task;
import me.raspolozi.services.TaskService;

@RestController
@RequestMapping("/api/v1/events/{eventId}")
public class TaskController {

	@Autowired
	private TaskService taskService;

	/**
	 * Gets tasks with matching event id.
	 * 
	 * @param id Tasks with passed event id should be returned.
	 * 
	 * @return ResponseEntity containing response status code information and List of Task objects.
	 */
	@RequestMapping(method=RequestMethod.GET,value="/tasks")
	public ResponseEntity<?> getAllTasks(@PathVariable int eventId) {
		List<Task> tasks = taskService.getAll(eventId);	
		
		if (tasks == null || tasks.isEmpty()) {
            return new ResponseEntity<List<Task>>(HttpStatus.NO_CONTENT);
        }
		
		return new ResponseEntity<List<Task>>(tasks, HttpStatus.OK);
	}
	
	/**
	 * Gets task with matching id.
	 * If Task object with passed id doesn't exist, 404 error status code will be returned.
	 * 
	 * @param id Task with passed id should be returned.
	 * 
	 * @return ResponseEntity containing response status code information and Task object.
	 */
	@RequestMapping(method=RequestMethod.GET,value="/tasks/{id}")
	public ResponseEntity<Task> getTask(@PathVariable int id){
		Task task = taskService.getTask(id);
		if(task == null)
			return new ResponseEntity<Task>(task, HttpStatus.NOT_FOUND);
		
		return new ResponseEntity<Task>(task, HttpStatus.OK);
	}
	
	/**
	 * Creates new task from provided Event object.
	 * If client provides Task object ID in request body, 400 error will be returned
	 * If Task object add fails, 400 error status code will be returned.
	 * 
	 * @param event Task object that should be added.
	 * 
	 * @return ResponseEntity containing response status code information.
	 */
	@RequestMapping(method=RequestMethod.POST,value="/tasks")
	public ResponseEntity<Task> addTask(@RequestBody Task task, @PathVariable int eventId) {
		// If client provides Task object ID in request body, 400 error will be returned
		if(task.getId() != 0)
			return new ResponseEntity<Task>(HttpStatus.BAD_REQUEST);
		
		task.setEvent(new Event(eventId));
		Task taskResult = taskService.addTask(task);
		
		if (taskResult == null)
		{
			return new ResponseEntity<Task>(taskResult, HttpStatus.BAD_REQUEST);
		}
				
		return new ResponseEntity<Task>(taskResult, HttpStatus.OK);
	}
	
	/**
	 * Replaces existing task with matching id with information from provided Task object.
	 * Task object fields that are not provided in the body will be nullified.
	 * Client will have to provide Task object without id.
	 * If Task object id is provided inside request body, 400 error status code will be returned.
	 * If Task object with passed id doesn't exist, 404 error status code will be returned.
	 * 
	 * @param event Task object body information, that should update Event object with matching id.
	 * @param id Task with passed id should be replaced.
	 * 
	 * @return ResponseEntity containing response status code information.
	 */
	@RequestMapping(method=RequestMethod.PUT,value="/tasks/{id}")
	public ResponseEntity<Task> updateTask(@RequestBody Task task, @PathVariable int eventId, @PathVariable int id) {
		// If client provides Task object ID in request body, 400 error will be returned
		if(task.getId() != 0)
			return new ResponseEntity<Task>(HttpStatus.BAD_REQUEST);
		
		task.setId(id);
		task.setEvent(new Event(eventId));
		Task taskResult = taskService.updateTask(task);
		
		if (taskResult == null)
		{
			return new ResponseEntity<Task>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Task>(taskResult, HttpStatus.OK);
	}
	
	/**
	 * Removes existing task with matching id. 
	 * If Task object with passed id doesn't exist, 404 error status code will be returned.
	 * 
	 * @param id Task with passed id should be deleted.
	 * @return ResponseEntity containing response status code information.
	 */
	@RequestMapping(method=RequestMethod.DELETE,value="/tasks/{id}")
	public ResponseEntity<Task> deleteTask(@PathVariable int id){
		try {
			taskService.deleteTask(id);
		}
		catch(EmptyResultDataAccessException e) {
			return new ResponseEntity<Task>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Task>(HttpStatus.OK);
	}
}