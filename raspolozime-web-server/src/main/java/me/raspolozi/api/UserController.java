package me.raspolozi.api;


import me.raspolozi.configuration.APIConfig;
import me.raspolozi.entities.User;
import me.raspolozi.exception.ExceptionResponse;
import me.raspolozi.exception.ResourceConflictException;
import me.raspolozi.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * RestController class for for requests to users collection.
 * 
 * @author Miloš Ranđelović
 *
 */
@RestController
@RequestMapping(APIConfig.USERS_URL)
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Value("${jwt.expires_in}")
	private int EXPIRES_IN;

	@Value("${jwt.cookie}")
	private String TOKEN_COOKIE;

	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET, value = "")
	public ResponseEntity<?> getUsers(@PageableDefault(size = 25) Pageable pageable) {
		Page<User> users = userService.getUsers(pageable);
		
		return ResponseEntity.ok(users);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<?> getUser(@PathVariable int id) {
		User user = userService.getUser(id);
		
		if(user == null) {
			ExceptionResponse response = new ExceptionResponse();
			response.setErrorCode("Bad request");
			response.setErrorMessage("User does not exists for id: " + id);
			
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
		
		return ResponseEntity.ok(user);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "")
	public ResponseEntity<?> addUser(@Validated @RequestBody User userRequest) {
		if (userRequest.getId() != null)
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		User existUser = userService.findUserByUsername(userRequest.getUsername());
		if (existUser != null) {
			throw new ResourceConflictException(userRequest.getId(), "Username already exists");
		}
		User user = userService.addUser(userRequest);

		return new ResponseEntity<User>(user, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.PATCH, value = "/{id}")
	public ResponseEntity<?> updateUser(@PathVariable int id, @RequestBody Map<String, Object> userRequest) {

		if (userRequest.isEmpty() == true) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		// If client provides Case object ID in request body, 400 error will be returned
		if (userRequest.containsKey("id")) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		// check if illegal parameter was provided
		for(String key : userRequest.keySet()) {
			try {
				User.class.getDeclaredField(key);
			} catch (NoSuchFieldException e) {
				logger.error("JSON could not be parsed. Wrong parameter provided for User object: " + key, e);
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			} catch (SecurityException e) {
				logger.error(e.getMessage(), e);
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		
		User user = userService.getUser(id);
		
		if(user == null) {
			ExceptionResponse response = new ExceptionResponse();
			response.setErrorCode("Bad request");
			response.setErrorMessage("User does not exists for id: " + id);
			
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
		
		user = userService.updateUserPartially(userRequest, user);
		
		return ResponseEntity.ok(user);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable int id) {

		User user = userService.getUser(id);
		
		if(user == null) {
			ExceptionResponse response = new ExceptionResponse();
			response.setErrorCode("Bad request");
			response.setErrorMessage("User does not exists for id: " + id);
			
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			userService.deleteUser(id);
		}
		catch(IllegalArgumentException | EmptyResultDataAccessException e) {
			ExceptionResponse response = new ExceptionResponse();
			response.setErrorCode("Bad request");
			response.setErrorMessage("User does not exists for id: " + id);
			
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
		
		return ResponseEntity.ok().build();
	}
	
}
