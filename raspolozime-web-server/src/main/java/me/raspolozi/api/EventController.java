package me.raspolozi.api;

import me.raspolozi.configuration.APIConfig;
import me.raspolozi.exception.ExceptionResponse;
import me.raspolozi.repository.EventSpecification;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

import me.raspolozi.entities.Event;
import me.raspolozi.services.EventService;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

@RestController
@RequestMapping(APIConfig.EVENTS_URL)
public class EventController {

	private static final Logger logger = LoggerFactory.getLogger(EventController.class);

	@Autowired
	private EventService eventService;

	/**
	 * Gets events matching pagination information.
	 * When using illegal sorting parameters, 400 error status code will be returned.
	 * 
	 * @param pageable Pagination information. Default size per page is 25.
	 * @return ResponseEntity containing response status code information and Pageable List of Event objects.
	 */
	@RequestMapping(method = RequestMethod.GET, value = "")
	public ResponseEntity<Page<Event>> getEvents(@PageableDefault(size = 25) Pageable pageable, Event event) {
		Page<Event> events = null;
		EventSpecification eventSpecification = new EventSpecification(event);
		
		try {
			events = eventService.getEvents(eventSpecification, pageable);
		}
		catch (PropertyReferenceException e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<Page<Event>>(HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<Page<Event>>(events, HttpStatus.OK);
	}

	/**
	 * Gets event with matching id.
	 * If Event object with passed id doesn't exist, 404 error status code will be returned.
	 * 
	 * @param id Event with passed id should be returned.
	 * 
	 * @return ResponseEntity containing response status code information and Event object.
	 */
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public ResponseEntity<Event> getEvent(@PathVariable int id){
		Event event = eventService.getEvent(id);
		
		if(event == null) {
			return new ResponseEntity<Event>(event, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Event>(event, HttpStatus.OK);
	}
	
	/**
	 * Creates new event from provided Event object.
	 * If client provides Event object ID in request body, 400 error will be returned
	 * If Event object add fails, 400 error status code will be returned.
	 * 
	 * @param event Event object that should be added.
	 * 
	 * @return ResponseEntity containing response status code information.
	 */
	@RequestMapping(method=RequestMethod.POST,value="")
	public ResponseEntity<?> addEvent(@RequestBody Event event) {
		// If client provides Event object ID in request body, 400 error will be returned
		if (event.getId() != 0) {
			return new ResponseEntity<Event>(HttpStatus.BAD_REQUEST);
		}
		
		Event eventResult = null;
		try {
			eventResult = eventService.addEvent(event);
		} catch(IllegalArgumentException e) {
			ExceptionResponse response = new ExceptionResponse();
			response.setErrorCode("Invalid request");
			response.setErrorMessage(e.getMessage());

			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}

		if (eventResult == null) {
			return new ResponseEntity<Event>(eventResult, HttpStatus.BAD_REQUEST);
		}
				
		return new ResponseEntity<Event>(eventResult, HttpStatus.CREATED);
	}
	
	/**
	 * Replaces existing event with matching id with information from provided Event object.
	 * Event object fields that are not provided in the body will be nullified.
	 * Client will have to provide Event object without id.
	 * If Event object id mismatches path variable, 400 error status code will be returned.
	 * If Event object with passed id doesn't exist, 404 error status code will be returned.
	 * If Event object was build from empty body, 400 error status code will be returned.
	 * 
	 * @param event Event object body information, that should update Event object with matching id.
	 * @param id Event with passed id should be replaced.
	 * 
	 * @return ResponseEntity containing response status code information.
	 */
	@RequestMapping(method=RequestMethod.PUT,value="/{id}")
	public ResponseEntity<?> updateEvent(@RequestBody Event event, @PathVariable int id) {
		// If client provides Event object ID in request body, 400 error will be returned
		if(event.getId() != 0) {
			return new ResponseEntity<Event>(HttpStatus.BAD_REQUEST);
		}
		
		event.setId(id);

		// If Event object was build from empty body, 400 error will be returned
		boolean isEmptyBody = false;
		for (Method m : event.getClass().getDeclaredMethods())
		{
			try {
				if (m.getName().startsWith("get") && !m.getName().equals("getId") && m.invoke(event) != null) {
					isEmptyBody = false;
				}
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				logger.error(e.getMessage(), e);
				return new ResponseEntity<Event>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		if (isEmptyBody) {
			return new ResponseEntity<Event>(HttpStatus.BAD_REQUEST);
		}
		
		Event eventResult = null;
		try {
			eventResult = eventService.updateEvent(event);
		} catch(IllegalArgumentException e) {
			ExceptionResponse response = new ExceptionResponse();
			response.setErrorCode("Invalid request");
			response.setErrorMessage(e.getMessage());

			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		
		if (eventResult == null)
		{
			return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Event>(eventResult, HttpStatus.OK);
	}


	/**
	 * Updates existing event with matching id with information from provided Event object.
	 * Event object fields that are not provided in the body will be ignored during update.
	 * Client will have to provide Event object with id, or it will be set to variable from path.
	 * If Event object id mismatches path variable, 400 error status code will be returned.
	 * If Event object with passed id doesn't exist, 404 error status code will be returned.
	 * If Event object was build from empty body, 400 error status code will be returned.
	 *
	 * @param event Map containing information about fields and values for Event object.
	 * @param id Event object id that should be updated.
	 *
	 * @return ResponseEntity containing response status code information.
	 */
	@RequestMapping(method = RequestMethod.PATCH, value = "/{id}")
	public ResponseEntity<Event> updateEvent(@RequestBody Map<String, Object> event, @PathVariable int id) {
		if (event.isEmpty() == true) {
			return new ResponseEntity<Event>(HttpStatus.BAD_REQUEST);
		}

		// If client provides Event object ID in request body, 400 error will be returned
		if (event.containsKey("id")) {
			return new ResponseEntity<Event>(HttpStatus.BAD_REQUEST);
		}

		// check if illegal parameter was provided
		for(String key : event.keySet())
		{
			try {
				Event.class.getDeclaredField(key);
			} catch (NoSuchFieldException e) {
				logger.error("JSON could not be parsed. Wrong parameter provided for Event object: " + key, e);
				return new ResponseEntity<Event>(HttpStatus.BAD_REQUEST);
			} catch (SecurityException e) {
				logger.error(e.getMessage(), e);
				return new ResponseEntity<Event>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		Event eventResult = eventService.updateEventPartially(event, id);

		if (eventResult == null)
		{
			return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Event>(eventResult, HttpStatus.OK);
	}
	
	/**
	 * Removes existing event with matching id. 
	 * If Event object with passed id doesn't exist, 404 error status code will be returned.
	 * If Event object with passed id is used in suite, 409 error status code will be returned.
	 * 
	 * @param id Event with passed id should be deleted.
	 * @return ResponseEntity containing response status code information.
	 */
	@RequestMapping(method=RequestMethod.DELETE,value="/{id}")
	public ResponseEntity<?> deleteEvent(@PathVariable int id){
		Event event = eventService.getEvent(id);
		if (event == null) {
			ExceptionResponse response = new ExceptionResponse();
			response.setErrorCode("Invalid request");
			response.setErrorMessage("Event doesn't exist for provided id: " + id);

			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}

		try {
			eventService.deleteEvent(event);
		} catch(EmptyResultDataAccessException e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Event>(HttpStatus.OK);
	}


	@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="JSON could not be parsed. Illegal parameter or value provided.")
	@ExceptionHandler({HttpMessageNotReadableException.class})
	public void eventSerializationError(Exception ex) {
		logger.error(ex.getMessage());
	}

	@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Could not convert provided ID value to numeric.")
	@ExceptionHandler({NumberFormatException.class})
	public void idParameterError(Exception ex) {
		logger.error(ex.getMessage());
	}

	@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Illegal or duplicate value provided.")
	@ExceptionHandler({ConstraintViolationException.class})
	public void constraintViolationError(Exception ex) {
		logger.error(ex.getMessage());
	}
}
