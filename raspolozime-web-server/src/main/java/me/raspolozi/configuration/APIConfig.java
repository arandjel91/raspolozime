package me.raspolozi.configuration;

public class APIConfig {

	public static final String EVENTS_PATH = "events";
	
	public static final String TASKS_PATH = "tasks";
	
	public static final String AUTH_PATH = "auth";
	
	public static final String SIGNUP_PATH = "signup";
	
	public static final String LOGIN_PATH = "login";
	
	public static final String LOGOUT_PATH = "logout";
	
	public static final String WHOAMI_PATH = "whoami";
	
	public static final String CHANGE_PASSWORD_PATH = "changePassword";
	
	public static final String REFRESH_PATH = "refresh";
	
	public static final String ADMIN_PATH = "admin";
	
	public static final String USERS_PATH = "users";
	
	
	public static final String API_VERSION = "v1";
	
	public static final String API_URL = "/api/" + API_VERSION;
	
	public static final String EVENTS_URL = API_URL + "/" + EVENTS_PATH;
	
	public static final String TASKS_URL = API_URL + "/" + TASKS_PATH;
	
	public static final String AUTH_URL = API_URL + "/" + AUTH_PATH;
	
	public static final String LOGIN_URL = AUTH_URL + "/" + LOGIN_PATH;
	
	public static final String LOGOUT_URL = AUTH_URL + "/" + LOGOUT_PATH;
	
	public static final String WHOAMI_URL = AUTH_URL + "/" + WHOAMI_PATH;
	
	public static final String CHANGE_PASSWORD_URL = AUTH_URL + "/" + CHANGE_PASSWORD_PATH;
	
	public static final String ADMIN_URL = API_URL + "/" + ADMIN_PATH;
	
	public static final String USERS_URL = API_URL + "/" + USERS_PATH;
}
