package me.raspolozi.configuration;

import me.raspolozi.entities.Role;
import me.raspolozi.entities.User;
import me.raspolozi.repository.RoleRepository;
import me.raspolozi.repository.UserRepository;
import me.raspolozi.services.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;
import java.util.Set;

@Configuration
public class StartDB {

  @Value("${default.admin.email}")
  private String defAdminEmail;

  @Value("${default.admin.password}")
  private String defAdminPassword;

  @Value("${default.admin.name}")
  private String defAdminName;

  @Value("${default.admin.lastname}")
  private String defAdminLastname;

  @Bean
  CommandLineRunner init(UserRepository userRepository, RoleRepository roleRepository, UserService userService) {
    return args -> {

      long countUsers = userRepository.count();

      if (countUsers == 0) {
        Role role = roleRepository.findByRole(Role.ROLE_ADMIN);
        if (role == null) {
          Role roleAdmin = new Role();
          roleAdmin.setRole(Role.ROLE_ADMIN);
          roleRepository.save(roleAdmin);
        }

        role = roleRepository.findByRole(Role.ROLE_USER);
        if (role == null) {
          Role roleUser = new Role();
          roleUser.setRole(Role.ROLE_USER);
          roleRepository.save(roleUser);
        }

        User user = new User();
        user.setActive(true);
        user.setEmail(defAdminEmail);
        user.setPassword(defAdminPassword);
        user.setName(defAdminName);
        user.setLastName(defAdminLastname);

        Set<Role> roles = new HashSet<Role>();
        roles.add((Role) roleRepository.findByRole(Role.ROLE_ADMIN));
        roles.add((Role) roleRepository.findByRole(Role.ROLE_USER));
        user.setRoles(roles);

        userService.addUser(user);
      }
    };
  }

}
