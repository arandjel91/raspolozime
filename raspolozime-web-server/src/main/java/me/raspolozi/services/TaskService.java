package me.raspolozi.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import me.raspolozi.entities.Task;
import me.raspolozi.repository.TaskRepository;

/**
 * Service class for manipulating over Task objects.
 * 
 * @author Miloš Ranđelović
 *
 */
@Service
public class TaskService {
	
	@Autowired
	private TaskRepository taskRepository;
	
	/**
	 * Gets tasks with matching event id.
	 * 
	 * @param eventId Tasks with passed event id should be returned.
	 * 
	 * @return List of tasks.
	 */
	public List<Task> getAll(int eventId){
		List<Task> tasks = new ArrayList<>();
		taskRepository.findByEventId(eventId).forEach(tasks::add);
		
		return tasks;
	}

	/**
	 * Gets task with matching id. 
	 * 
	 * @param id Task with passed id should be returned.
	 * 
	 * @return Task object with matching id, or null if such is not found.
	 */
	public Task getTask(int id) {
		return taskRepository.findById(id).orElse(null);
	}

	/**
	 * Creates new task from provided Task object.
	 * 
	 * @param task Task object that should be added.
	 * 
	 * @return Added Task object.
	 */
	public Task addTask(Task task) {
		return taskRepository.save(task);
	}

	/**
	 * Replaces existing task with matching id with information from provided Task object.
	 * Task object fields that are not provided in the body will be nullified.
	 * 
	 * @param task Task object that should be updated.
	 * 
	 * @return Replaced Task object.
	 */
	public Task updateTask(Task task) {
		Task taskResult = taskRepository.findById(task.getId()).orElse(null);
		
		if (taskResult == null)
		{
			return null;
		}
		
		return taskRepository.save(task);
	}

	/**
	 * Removes existing task with matching id.
	 * 
	 * @param id Task with passed id should be deleted.
	 * @throws EmptyResultDataAccessException Data access exception thrown when a result was expected to have at least one row (or element) but zero rows (or elements) were actually returned.
	 */
	public void deleteTask(int id) {
		taskRepository.delete(getTask(id));
	}
}
