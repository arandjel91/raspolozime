package me.raspolozi.services;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import me.raspolozi.entities.Role;
import me.raspolozi.entities.User;
import me.raspolozi.repository.RoleRepository;
import me.raspolozi.repository.UserRepository;

/**
 * Service class for managing user and user related configuration.
 * 
 * @author Miloš Ranđelović
 *
 */
@Service("userService")
public class UserService implements UserDetailsService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	/**
	 * Gets user with matching username.
	 * 
	 * @param username User with passed username should be returned.
	 * @return User with matching username, or null if such is not found.
	 */
	public User findUserByUsername(String username) throws UsernameNotFoundException {
		return userRepository.findByEmail(username);
	}

	/**
	 * Gets user with matching id.
	 *
	 * @param id User with passed id should be returned.
	 * @return User with matching id, or null if such is not found.
	 */
	public User getUser(int id) throws AccessDeniedException {
		User user = userRepository.findById(id).orElse(null);
		return user;
	}

	/**
	 * Gets all existing users.
	 * 
	 * @return Page of all existing users.
	 */
	public Page<User> getUsers(Pageable pageable) throws AccessDeniedException {
		int page = pageable.getPageNumber();
		int size = pageable.getPageSize();

		if(size > 100) {
			size = 100;
		}

		return userRepository.findAll(PageRequest.of(page, size, pageable.getSort()));
	}

	public User deleteUser(int id) throws IllegalArgumentException {
		User user = userRepository.findById(id).orElse(null);
		userRepository.delete(user);

		return user;
	}

	/**
	 * Creates new user from provided User object.
	 *
	 * @param user User object that should be added.
	 * @throws NullPointerException If the user parameter is null
	 * @throws RuntimeException If an unexpected exception occurs during compilation.
	 * @return Added User object.
	 */
	public User addUser(User user) {
		if (user == null) {
			throw new NullPointerException("User parameter cannot be null");
		}

		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		if (user.getActive() == null) {
			user.setActive(false);
		}
		if (user.getRoles() == null) {
			Role userRole = roleRepository.findByRole("ROLE_USER");
			user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		}

		return userRepository.save(user);
	}

	public User updateUserPartially(Map<String, Object> userRequest, User user) {

		userRequest.put("id", user.getId());

		if(userRequest.containsKey("password")) {
			user.setPassword(bCryptPasswordEncoder.encode(userRequest.get("password").toString()));
		}

		for(String key : userRequest.keySet()) {
			for (Method methodGetter : user.getClass().getDeclaredMethods()) {
				try {
					if (methodGetter.getName().startsWith("get")
							&& !methodGetter.getName().equals("getId")
							&& !methodGetter.getName().equals("getPassword")
							&& methodGetter.getName().substring(3).toLowerCase().equals(key.toLowerCase())
						) {
						String setter = "set" + methodGetter.getName().substring(3);
						Method methodSetter = user.getClass().getMethod(setter, methodGetter.getReturnType());

						methodSetter.invoke(user, userRequest.get(key));
					}
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
					logger.error(e.getMessage(), e);
					return null;
				}
			}
		}

		return userRepository.save(user);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(username);
		if (user == null) {
			throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
		} else {
			return user;
		}
	}

	/**
	 * Change user's password.
	 * 
	 * @param oldPassword Old user's password.
	 * @param newPassword New user's password.
	 * @return <code>true</code> if password was changed successfully, or <code>false</code> otherwise.
	 */
	public boolean changePassword(String oldPassword, String newPassword) {
		Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
		String username = currentUser.getName();

		if (authenticationManager != null) {
			logger.debug("Re-authenticating user '" + username + "' for password change request.");

			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, oldPassword));
		} else {
			logger.debug("No authentication manager set. can't change Password!");

			return false;
		}

		logger.debug("Changing password for user '" + username + "'");

		User user = (User) loadUserByUsername(username);

		user.setPassword(bCryptPasswordEncoder.encode(newPassword));
		user = userRepository.save(user);
		
		if (user != null) {
			return false;
		}
		
		return true;
	}
}