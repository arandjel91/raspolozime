package me.raspolozi.services;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import me.raspolozi.repository.EventSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;

import me.raspolozi.entities.Event;
import me.raspolozi.repository.EventRepository;

/**
 * Service class for manipulating over Event objects.
 * 
 * @author Miloš Ranđelović
 *
 */
@Service
public class EventService {

	private static final Logger logger = LoggerFactory.getLogger(EventService.class);

	@Autowired
	private EventRepository eventRepository;
	
	/**
	 * Gets events matching matching pagination information.
	 * 
	 * @param pageable Pagination information.
	 * @throws PropertyReferenceException Exception thrown when using illegal sorting parameters.
	 * 
	 * @return Page containing Pageable List of Event objects.
	 */	
	public Page<Event> getEvents(EventSpecification eventSpecification, Pageable pageable) {
		int page = pageable.getPageNumber();
		int size = pageable.getPageSize();
		
		if(size > 50) {
			size = 50;
		}
		
		return eventRepository.findAll(eventSpecification, PageRequest.of(page, size, pageable.getSort()));
	}

	/**
	 * Gets event with matching id. 
	 * 
	 * @param id Event with passed id should be returned.
	 * 
	 * @return Event object with matching id, or null if such is not found.
	 */
	public Event getEvent(int id) {	return eventRepository.findById(id).orElse(null);	}

	/**
	 * Creates new event from provided Event object.
	 * 
	 * @param event Event object that should be added.
	 * 
	 * @return Added Event object.
	 */
	public Event addEvent(Event event) throws IllegalArgumentException {
		if(event == null) {
			throw new IllegalArgumentException("Event cannot be null.");
		}

		return eventRepository.save(event);
	}

	/**
	 * Replaces existing event with matching id with information from provided Event object.
	 * Event object fields that are not provided in the body will be nullified.
	 * 
	 * @param event Event object that should be updated.
	 * 
	 * @return Replaced Event object.
	 */
	public Event updateEvent(Event event) {
		if(event == null)
			throw new IllegalArgumentException("Event cannot be null.");
		
		Event eventResult = getEvent(event.getId());
		
		if (eventResult == null)
		{
			return null;
		}
		event.setTasks(eventResult.getTasks());
		
		return eventRepository.save(event);
	}


	/**
	 * Updates existing event with matching event id with information from provided Event object.
	 * Event object fields that are not provided in the body will be ignored during update.
	 *
	 * @param eventBody Map containing information about fields and values for Event object.
	 * @param id Event object id that should be updated.
	 *
	 * @return Updated Event object.
	 */
	public Event updateEventPartially(Map<String, Object> eventBody, int id) {
		Event eventResult = eventRepository.findById(id).orElse(null);

		if (eventResult == null)
		{
			return null;
		}

		for(String key : eventBody.keySet())
		{
			for (Method methodGetter : eventResult.getClass().getDeclaredMethods())
			{
				try {
					if (methodGetter.getName().startsWith("get") && !methodGetter.getName().equals("getId")
							&& methodGetter.getName().substring(3).toLowerCase().equals(key.toLowerCase()))
					{
						String setter = "set" + methodGetter.getName().substring(3);
						Method methodSetter = eventResult.getClass().getMethod(setter, methodGetter.getReturnType());

						methodSetter.invoke(eventResult, eventBody.get(key));
					}
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
					logger.error(e.getMessage(), e);
					return null;
				}
			}
		}
		eventResult = eventRepository.save(eventResult);

		return eventResult;
	}

	/**
	 * Removes existing event with matching id.
	 * 
	 * @param event Event that should be deleted.
	 * @throws EmptyResultDataAccessException Data access exception thrown when a result was expected to have at least one row (or element) but zero rows (or elements) were actually returned.
	 */
	public void deleteEvent(Event event) { eventRepository.delete(event); }

	/**
	 * Get events marked as top events.
	 * 
	 * @return List of event objects.
	 */
	public List<Event> getTopEvents() {
		List<Event> events = new ArrayList<>();
		events = eventRepository.findByTopEvent(true);
		
		return events;
	}
	
	/**
	 * Get events created by passed owner.
	 * 
	 * @param email Owner of the events.
	 * @return List of event objects with specified owner. 
	 */
	public List<Event> getEventsByOwner(String email) {
		List<Event> events = new ArrayList<>();
		events = eventRepository.findByOwner(email);
		
		return events;
	}
}
