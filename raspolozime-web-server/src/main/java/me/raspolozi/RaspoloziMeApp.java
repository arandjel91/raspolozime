package me.raspolozi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

/**
 * Spring boot main class for running application.
 * 
 * @author Miloš Ranđelović
 *
 */
@PropertySource(value = {"classpath:raspolozime.properties"})
@SpringBootApplication
public class RaspoloziMeApp {

	public static void main(String[] args) {
		SpringApplication.run(RaspoloziMeApp.class, args);
	}

}
