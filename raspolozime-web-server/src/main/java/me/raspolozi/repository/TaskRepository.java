package me.raspolozi.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import me.raspolozi.entities.Task;

/**
 * JPA Repository for database work over Task object.
 * 
 * @author Miloš Ranđelović
 */
public interface TaskRepository extends CrudRepository<Task, Integer>{
	
	public List<Task> findByEventId(int eventId);

}
