package me.raspolozi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import me.raspolozi.entities.Event;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * JPA Repository for database work over Event object.
 * 
 * @author Miloš Ranđelović
 */
public interface EventRepository extends JpaRepository<Event, Integer>, JpaSpecificationExecutor<Event> {

	public List<Event> findByTopEvent(Boolean topEvent);
	
	//@Query(value = "select * from Event e where e.person_user_id in (select u.user_id from user u where u.email=?1)", nativeQuery = true)
	//List<Event> findByEmailAddress(String emailAddress);
	
	public List<Event> findByOwner(String emailAddress);
}
