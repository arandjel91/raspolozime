package me.raspolozi.repository;

import me.raspolozi.entities.Event;
import me.raspolozi.entities.Event_;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class EventSpecification implements Specification<Event> {

	private static final long serialVersionUID = -5140529434766998899L;

	private final Event criteria;

	public EventSpecification(Event criteria) {
        this.criteria=criteria;
    }
	
	@Override
	public Predicate toPredicate(Root<Event> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		final List<Predicate> predicates = new ArrayList<Predicate>();
		
        if(criteria.getTitle() != null) {
            predicates.add(cb.like(cb.upper(root.get(Event_.title)), criteria.getTitle().toUpperCase()));
        }
        if(criteria.getDescription() != null) {
            predicates.add(cb.like(cb.upper(root.get(Event_.description)), criteria.getDescription()));
        }
        if(criteria.getLocation() != null) {
            predicates.add(cb.equal(root.get(Event_.location), criteria.getLocation()));
        }
        if(criteria.isTopEvent() != null) {
            predicates.add(cb.equal(root.get(Event_.topEvent), criteria.isTopEvent()));
        }
        if(criteria.getOwner() != null) {
            predicates.add(cb.like(cb.upper(root.get(Event_.owner)), criteria.getOwner().toUpperCase()));
        }
        
        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
	}

}
