package me.raspolozi.entities;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Class presenting Event entity. Event is a Bean, meaning that it contains
 * private fields with getters and setters as well as default constructor.
 * 
 * @author Miloš Ranđelović
 *
 */
@Entity
public class Event {

	@Id
	@GeneratedValue
	private int id;
	
	private String title;
	
	private String description;
	
	private String location;
	
	private Boolean topEvent;
	
	private String owner;

	@Temporal(TemporalType.TIMESTAMP)
	private Date eventDate;
	
	@OneToMany(mappedBy="event", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Task> tasks;

	public Event() {
	}

	public Event(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Boolean isTopEvent() {
		return topEvent;
	}

	public void setTopEvent(Boolean topEvent) {
		this.topEvent = topEvent;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String person) {
		this.owner = person;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
}
