package me.raspolozi.entities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Event.class)
public class Event_ {
	
	public static volatile SingularAttribute<Event, Integer> id;
	public static volatile SingularAttribute<Event, String> title;
	public static volatile SingularAttribute<Event, String> description;
	public static volatile SingularAttribute<Event, String> location;
	public static volatile SingularAttribute<Event, Boolean> topEvent;
	public static volatile SingularAttribute<Event, String> owner;

}
