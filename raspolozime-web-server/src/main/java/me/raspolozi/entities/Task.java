package me.raspolozi.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Class presenting Task entity. Task is a Bean, meaning that it contains
 * private fields with getters and setters as well as default constructor.
 * 
 * @author Miloš Ranđelović
 *
 */
@Entity
public class Task {

	@Id
	@GeneratedValue
	private int id;
	
	private String title;
	
	private String description;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="event_id")
	private Event event;
	
	public Task() {
	}

	public Task(int id, String title, String description) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

}
