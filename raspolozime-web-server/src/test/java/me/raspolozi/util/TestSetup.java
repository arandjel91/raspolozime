package me.raspolozi.util;

import me.raspolozi.entities.Role;
import me.raspolozi.entities.User;
import me.raspolozi.repository.RoleRepository;
import me.raspolozi.repository.UserRepository;
import me.raspolozi.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TestSetup {

	@Autowired
    private UserRepository userRepository;
	
	@Autowired
    private UserService userService;

	@Autowired
	private RoleRepository roleRepository;

	
	public void setupUsersAndRoles() {
		setupRole("ROLE_USER");
		setupRole("ROLE_ADMIN");
    	setupTestUser("test@raspolozi.me", "12345", Stream.of("ROLE_USER", "ROLE_ADMIN").collect(Collectors.toSet()));
    	setupTestUser("testsecond@raspolozi.me", "12345", Stream.of("ROLE_USER", "ROLE_ADMIN").collect(Collectors.toSet()));
	}
	
	public void cleanupUsersAndRoles() {
		userRepository.deleteAll();
		roleRepository.deleteAll();
	}
	 
	private void setupRole(String roleName) {
		Role role = roleRepository.findByRole(roleName);
		
		if(role == null)
		{	
			role = new Role();
			
			role.setRole(roleName);
			roleRepository.save(role);
		}
	}

	private void setupTestUser(String username, String password, Set<String> roles) {
		User user = new User();
		user.setEmail(username);
		user.setPassword(password);
		user.setName("TestUserName");
		user.setLastName("TestUserLastName");
		
		userService.addUser(user);
		
		HashSet<Role> userRoles = new HashSet<Role>();
		
		for(String role: roles) {
			Role userRole = roleRepository.findByRole(role);
			userRoles.add(userRole);
		}
		user.setRoles(userRoles);
		user.setActive(true);
		userRepository.save(user);
    }
}
