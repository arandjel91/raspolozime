package me.raspolozi.api.event;

import static org.junit.Assert.assertNull;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;

import me.raspolozi.configuration.APIConfig;
import me.raspolozi.util.TestSetup;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.jayway.jsonpath.JsonPath;

import me.raspolozi.RaspoloziMeApp;
import me.raspolozi.configuration.SecurityConfiguration;
import me.raspolozi.entities.Event;
import me.raspolozi.repository.EventRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RaspoloziMeApp.class)
@WebAppConfiguration
@ContextConfiguration(classes = SecurityConfiguration.class)
public class EventAPIDeleteTest {

	@Autowired
	private TestSetup testSetup;

	private static final MediaType contentType = new MediaType(
			MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), 
			Charset.forName("utf8")
			);
	
	private MockMvc mockMvc;
	private ResultActions actions;
	private MvcResult mvcResult;
	
	private int eventId;
	
	private String accessToken;
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	@Autowired
	private EventRepository eventRepository;
	
	@Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();

		testSetup.setupUsersAndRoles();
	}

	@After
	public void cleanup() throws Exception {
		eventRepository.deleteAll();

		logoutUser();

		testSetup.cleanupUsersAndRoles();
	}
	
	// Scenario: Delete event
	@Test
	public void deleteEvent() throws Exception {
		givenUserAuthorized();
		givenEventExists();
		whenDeleteEvent();
		thenStatusOK();
		thenEventNotExists();
		
		logoutUser();
	}

	// Scenario: Delete event not authenticated
	@Test
	public void deleteEventNotAuthenticated() throws Exception {
		givenUserNotAuthenticated();
		whenDeleteEvent();
		thenStatusUnauthorized();
	}

	// Scenario: Delete event non existing
	@Test
	public void deleteEventNonExisting() throws Exception {
		givenUserAuthorized();
		givenEventNotExists();
		whenDeleteEvent();
		thenStatusNotFound();
		
		logoutUser();
	}

	private void givenUserAuthorized() throws Exception {
		String email = "test@raspolozi.me";
		String password = "12345";
		mvcResult = mockMvc.perform(formLogin(APIConfig.LOGIN_URL)
									.user("email", email)
									.password("password", password))
									.andReturn();

		String jsonResult = mvcResult.getResponse().getContentAsString();
		
		if (jsonResult == null || jsonResult.isEmpty())
			throw new Exception("Could not login as user: " + email);
		
		accessToken = JsonPath.read(jsonResult, "$.access_token");
	}

	private void givenUserNotAuthenticated() {
		accessToken = null;
	}
	
	private void givenEventExists() throws Exception {
		Event newEvent = new Event();
		newEvent = eventRepository.save(newEvent);
		
		if (newEvent == null) {
			throw new Exception("Could not create new event");
		}
		
		eventId = newEvent.getId();
	}
	
	private void givenEventNotExists() {
		eventId = -1;
	}
	
	private void whenDeleteEvent() throws Exception {
		actions = mockMvc.perform(delete(APIConfig.EVENTS_URL + "/" + eventId)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType));
	}
	 
	private void thenStatusOK() throws Exception {
		actions.andExpect(status().isOk());
	}
	
	private void thenStatusNotFound() throws Exception {
		actions.andExpect(status().isNotFound());
	}
	
	private void thenStatusUnauthorized() throws Exception {
		actions.andExpect(status().isUnauthorized());
	}

	private void thenEventNotExists() {
		Event deletedEvent = eventRepository.findById(eventId).orElse(null);
		assertNull("Event [id=" + eventId + "] has not been deleted", deletedEvent);
	}
	
	private void logoutUser() throws Exception {
		actions = mockMvc.perform(logout());
	}
}