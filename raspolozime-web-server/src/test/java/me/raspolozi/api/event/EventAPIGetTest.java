package me.raspolozi.api.event;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.ArrayList;

import me.raspolozi.configuration.APIConfig;
import me.raspolozi.util.TestSetup;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.jayway.jsonpath.JsonPath;

import me.raspolozi.RaspoloziMeApp;
import me.raspolozi.configuration.SecurityConfiguration;
import me.raspolozi.entities.Event;
import me.raspolozi.repository.EventRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RaspoloziMeApp.class)
@WebAppConfiguration
@ContextConfiguration(classes = SecurityConfiguration.class)
public class EventAPIGetTest {

	@Autowired
	private TestSetup testSetup;

	private static final MediaType contentType = new MediaType(
			MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8")
	);

	private MockMvc mockMvc;
	private ResultActions actions;
	private MvcResult mvcResult;

	private ArrayList<Integer> eventsIdentifiers;

	private String accessToken;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private EventRepository eventRepository;

	@Before
	public void setup() throws Exception {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();

		testSetup.setupUsersAndRoles();
	}

	@After
	public void cleanup() throws Exception {
		eventRepository.deleteAll();

		logoutUser();

		testSetup.cleanupUsersAndRoles();
	}

	// Scenario: Retrieve events
	@Test
	public void getEvents() throws Exception {
		givenUserAuthorized();
		givenEventsExists(25);
		whenGetEvents();
		thenStatusOK();
		thenReceivesFirstPage();
		thenFieldsSetForEachEvent(0, 25);
	}

	// Scenario: Retrieve events second page
	@Test
	public void getEventsSecondPage() throws Exception {
		givenUserAuthorized();
		givenEventsExists(50);
		whenGetEventsSecondPage();
		thenStatusOK();
		thenReceivesSecondPage();
		thenFieldsSetForEachEvent(1, 25);
	}

	// Scenario: Retrieve events last page
	@Test
	public void getEventsLastPage() throws Exception {
		givenUserAuthorized();
		givenEventsExists(60);
		whenGetEventsLastPage();
		thenStatusOK();
		thenReceivesLastPage();
		thenFieldsSetForEachEvent(2, 10);
	}

	// Scenario: Retrieve events 50
	@Test
	public void getEventsSize50() throws Exception {
		givenUserAuthorized();
		givenEventsExists(50);
		whenGetEventsSize50();
		thenStatusOK();
		thenReceivesEventsSize50();
		thenFieldsSetForEachEvent(0, 50);
	}

	// Scenario: Retrieve events over 50
	@Test
	public void getEventsSizeOver50() throws Exception {
		givenUserAuthorized();
		givenEventsExists(150);
		whenGetEventsSizeOver50();
		thenStatusOK();
		thenReceivesEventsSize50TotalOver50();
		thenFieldsSetForEachEvent(0, 50);
	}

	// Scenario: Retrieve events not authenticated
	@Test
	public void getEventsNotAuthenticated() throws Exception {
		givenUserNotAuthenticated();
		whenGetEvents();
		thenStatusOK();
	}

	// Scenario: Retrieve specific event
	@Test
	public void getEvent() throws Exception {
		givenUserAuthorized();
		givenEventsExists(1);
		whenGetEvent();
		thenStatusOK();
		thenFieldsSet();
	}

	// Scenario: Retrieve specific event non existing
	@Test
	public void getEventNonExisting() throws Exception {
		givenUserAuthorized();
		givenEventNotExists();
		whenGetEvent();
		thenStatusNotFound();
	}

	// Scenario: Filter Events
	@Test
	public void getEventsFilter() throws Exception {
		givenUserAuthorized();
		givenEventsExists(50);
		whenGetEventsFilterByTitle();
		thenStatusOK();
		thenReceivesEventsSize11();
		thenBodyContentMatchingTitle();
	}

	// Scenario: Filter Events Multiple Fields
	@Test
	public void getEventsFilterMultipleFields() throws Exception {
		givenUserAuthorized();
		givenEventsExists(50);
		whenGetEventsFilterByAllFields();
		thenStatusOK();
		thenReceivesEventsSize11();
		thenBodyContentMatchingAllFields();
	}

	// Scenario: Filter Events No Match
	@Test
	public void getEventsFilterNoMatch() throws Exception {
		givenUserAuthorized();
		givenEventsExists(50);
		whenGetEventsFilterNoMatch();
		thenStatusOK();
		thenBodyContentEmpty();
	}

	private void givenUserAuthorized() throws Exception {
		String email = "test@raspolozi.me";
		String password = "12345";
		mvcResult = mockMvc.perform(formLogin(APIConfig.LOGIN_URL)
				.user("email", email)
				.password("password", password))
				.andReturn();

		String jsonResult = mvcResult.getResponse().getContentAsString();

		if (jsonResult == null || jsonResult.isEmpty()) {
			throw new Exception("Could not login as user: " + email);
		}

		accessToken = JsonPath.read(jsonResult, "$.access_token");
	}

	private void givenUserNotAuthenticated() throws Exception {
		accessToken = null;
	}

	private void givenEventsExists(int numberOfEventsForAdd) throws Exception{
		eventsIdentifiers = new ArrayList<Integer>();

		for(int i = 0; i < numberOfEventsForAdd; i++) {
			Event event = new Event();
			event.setTitle("GETTEST" + i);
			event.setTopEvent(false);
			event.setDescription("GETTEST description");
			event.setLocation("Krusevac");
			event.setOwner("Batman");
			Event result = eventRepository.save(event);

			if (result == null) {
				throw new Exception("Event wasn't added at loop number: " + i);
			}

			eventsIdentifiers.add(result.getId());
		}
	}

	private void givenEventNotExists() {
		eventsIdentifiers = new ArrayList<Integer>();
		eventsIdentifiers.add(-1);
	}

	private void whenGetEvents() throws Exception {
		actions = mockMvc.perform(get(APIConfig.EVENTS_URL)
				.header("Authorization", "Bearer " + accessToken)
				.contentType(contentType));
	}

	private void whenGetEventsSecondPage() throws Exception {
		actions = mockMvc.perform(get(APIConfig.EVENTS_URL + "?page=1")
				.header("Authorization", "Bearer " + accessToken)
				.contentType(contentType));
	}

	private void whenGetEventsLastPage() throws Exception {
		actions = mockMvc.perform(get(APIConfig.EVENTS_URL + "?page=2")
				.header("Authorization", "Bearer " + accessToken)
				.contentType(contentType));
	}

	private void whenGetEventsSize50() throws Exception {
		actions = mockMvc.perform(get(APIConfig.EVENTS_URL + "?size=50")
				.header("Authorization", "Bearer " + accessToken)
				.contentType(contentType));
	}

	private void whenGetEventsSizeOver50() throws Exception {
		actions = mockMvc.perform(get(APIConfig.EVENTS_URL + "?size=120")
				.header("Authorization", "Bearer " + accessToken)
				.contentType(contentType));
	}

	private void whenGetEvent() throws Exception {
		int eventId = eventsIdentifiers.get(0);
		actions = mockMvc.perform(get(APIConfig.EVENTS_URL + "/" + eventId)
				.header("Authorization", "Bearer " + accessToken)
				.contentType(contentType));
	}

	private void whenGetEventsFilterByTitle() throws Exception {
		String filter = "title=GETTEST1%";
		actions = mockMvc.perform(get(APIConfig.EVENTS_URL + "?" + filter)
				.header("Authorization", "Bearer " + accessToken)
				.contentType(contentType));

	}

	private void whenGetEventsFilterByAllFields() throws Exception {
		String filter = "title=%TEST1%"
				+ "&description=GETTEST%"
				+ "&topEvent=false"
				+ "&location=Krusevac"
				+ "&owner=Batman"
				;

		actions = mockMvc.perform(get(APIConfig.EVENTS_URL + "?" + filter)
				.header("Authorization", "Bearer " + accessToken)
				.contentType(contentType));

	}

	private void whenGetEventsFilterNoMatch() throws Exception {
		String filter = "title=qwertyzxc";
		actions = mockMvc.perform(get(APIConfig.EVENTS_URL + "?" + filter)
				.header("Authorization", "Bearer " + accessToken)
				.contentType(contentType));
	}

	private void thenStatusOK() throws Exception {
		actions.andExpect(status().isOk());
	}

	private void thenStatusNotFound() throws Exception {
		actions.andExpect(status().isNotFound());
	}

	private void thenStatusUnauthorized() throws Exception {
		actions.andExpect(status().isUnauthorized());
	}

	private void thenReceivesFirstPage() throws Exception {
		actions
				.andExpect(jsonPath("$.first", is(true)))
				.andExpect(jsonPath("$.number", is(0)))
				.andExpect(jsonPath("$.size", is(25)))
				.andExpect(jsonPath("$.numberOfElements", is(25)))
				.andExpect(jsonPath("$.totalElements", is(25)));
	}

	private void thenReceivesSecondPage() throws Exception {
		actions
				.andExpect(jsonPath("$.first", is(false)))
				.andExpect(jsonPath("$.number", is(1)))
				.andExpect(jsonPath("$.size", is(25)))
				.andExpect(jsonPath("$.numberOfElements", is(25)))
				.andExpect(jsonPath("$.totalElements", is(50)));
	}

	private void thenReceivesLastPage() throws Exception {
		actions
				.andExpect(jsonPath("$.last", is(true)))
				.andExpect(jsonPath("$.number", is(2)))
				.andExpect(jsonPath("$.size", is(25)))
				.andExpect(jsonPath("$.numberOfElements", is(10)))
				.andExpect(jsonPath("$.totalElements", is(60)));
	}

	private void thenReceivesEventsSize50() throws Exception {
		actions
				.andExpect(jsonPath("$.first", is(true)))
				.andExpect(jsonPath("$.number", is(0)))
				.andExpect(jsonPath("$.size", is(50)))
				.andExpect(jsonPath("$.numberOfElements", is(50)))
				.andExpect(jsonPath("$.totalElements", is(50)));
	}

	private void thenReceivesEventsSize50TotalOver50() throws Exception {
		actions
				.andExpect(jsonPath("$.first", is(true)))
				.andExpect(jsonPath("$.number", is(0)))
				.andExpect(jsonPath("$.size", is(50)))
				.andExpect(jsonPath("$.numberOfElements", is(50)))
				.andExpect(jsonPath("$.totalElements", is(150)));
	}

	private void thenReceivesEventsSize11() throws Exception {
		actions
				.andExpect(jsonPath("$.first", is(true)))
				.andExpect(jsonPath("$.number", is(0)))
				.andExpect(jsonPath("$.size", is(25)))
				.andExpect(jsonPath("$.numberOfElements", is(11)))
				.andExpect(jsonPath("$.totalElements", is(11)));
	}

	private void thenFieldsSetForEachEvent(int page, int numberOfPageElements) throws Exception {
		for(int i = 0, iterator = i+page*25; i < numberOfPageElements; i++, iterator = i+page*25) {
			int eventId = eventsIdentifiers.get(iterator);
			actions
					.andExpect(jsonPath("$.content[" + i + "].id").value(is(eventId)))
					.andExpect(jsonPath("$.content[" + i + "].title").value(is("GETTEST" + iterator)))
					.andExpect(jsonPath("$.content[" + i + "].topEvent").value(is(false)))
					.andExpect(jsonPath("$.content[" + i + "].description").value(is("GETTEST description")))
					.andExpect(jsonPath("$.content[" + i + "].location").value(is("Krusevac")))
					.andExpect(jsonPath("$.content[" + i + "].owner").value(is("Batman")))
			;
		}
	}

	private void thenFieldsSet() throws Exception {
		int eventId = eventsIdentifiers.get(0);
		actions
				.andExpect(jsonPath("$.id", is(eventId)))
				.andExpect(jsonPath("$.title", is("GETTEST" + 0)))
				.andExpect(jsonPath("$.topEvent").value(is(false)))
				.andExpect(jsonPath("$.description", is("GETTEST description")))
				.andExpect(jsonPath("$.location", is("Krusevac")))
				.andExpect(jsonPath("$.owner", is("Batman")))
		;
	}

	private void thenBodyContentMatchingTitle() throws Exception {
		actions
				.andExpect(jsonPath("$.content", hasSize(11)));

		for(int i = 0; i < 11; i++) {
			actions
					.andExpect(jsonPath("$.content[" + i + "].title").value(Matchers.startsWith("GETTEST1")))
					.andExpect(jsonPath("$.content[" + i + "].topEvent").value(is(false)))
					.andExpect(jsonPath("$.content[" + i + "].description").value(is("GETTEST description")))
					.andExpect(jsonPath("$.content[" + i + "].location").value(is("Krusevac")))
					.andExpect(jsonPath("$.content[" + i + "].owner").value(is("Batman")))
			;
		}
	}

	private void thenBodyContentMatchingAllFields() throws Exception {
		actions
				.andExpect(jsonPath("$.content", hasSize(11)));

		for(int i = 0; i < 11; i++) {
			actions
					.andExpect(jsonPath("$.content[" + i + "].title").value(Matchers.startsWith("GETTEST1")))
					.andExpect(jsonPath("$.content[" + i + "].topEvent").value(is(false)))
					.andExpect(jsonPath("$.content[" + i + "].description").value(Matchers.containsString("description")))
					.andExpect(jsonPath("$.content[" + i + "].location").value(is("Krusevac")))
					.andExpect(jsonPath("$.content[" + i + "].owner").value(is("Batman")))
			;
		}
	}

	private void thenBodyContentEmpty() throws Exception {
		actions
				.andExpect(jsonPath("$.content", hasSize(0)))
				.andExpect(jsonPath("$.first", is(true)))
				.andExpect(jsonPath("$.number", is(0)))
				.andExpect(jsonPath("$.size", is(25)))
				.andExpect(jsonPath("$.numberOfElements", is(0)))
				.andExpect(jsonPath("$.totalElements", is(0)));
	}

	private void logoutUser() throws Exception {
		actions = mockMvc.perform(logout());
	}
}