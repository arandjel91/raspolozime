package me.raspolozi.api.event;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;

import me.raspolozi.configuration.APIConfig;
import me.raspolozi.util.TestSetup;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import me.raspolozi.RaspoloziMeApp;
import me.raspolozi.configuration.SecurityConfiguration;
import me.raspolozi.entities.Event;
import me.raspolozi.repository.EventRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RaspoloziMeApp.class)
@WebAppConfiguration
@ContextConfiguration(classes = SecurityConfiguration.class)
public class EventAPIPostTest {

	@Autowired
	private TestSetup testSetup;

	private static final MediaType contentType = new MediaType(
			MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), 
			Charset.forName("utf8")
			);
	
	private MockMvc mockMvc;
	private ResultActions actions;
	private MvcResult mvcResult;
	
	private String accessToken;
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	@Autowired
	private EventRepository eventRepository;
	
	@Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();

		testSetup.setupUsersAndRoles();
	}

	@After
	public void cleanup() throws Exception {
		eventRepository.deleteAll();

		logoutUser();

		testSetup.cleanupUsersAndRoles();
	}
	
	// Scenario: Create event
	@Test
	public void addEvent() throws Exception {
		givenUserAuthorized();
		whenAddEvent();
		thenStatusCreated();
		thenIdGenerated();
		thenFieldsEmpty();
		
		deleteEventAfterAdd();
		logoutUser();
	}
	
	// Scenario: Create event set fields
	@Test
	public void addEventSetFields() throws Exception {
		givenUserAuthorized();
		whenAddEventSetFields();
		thenStatusCreated();
		thenIdGenerated();
		thenFieldsSet();
		
		deleteEventAfterAdd();
		logoutUser();
	}
	
	// Scenario: Create event not authenticated
	@Test
	public void addEventNotAuthenticated() throws Exception {	
		givenUserNotAuthenticated();
		whenAddEvent();
		thenStatusUnauthorized();
	}
	
	// Scenario: Create event read only field
	@Test
	public void addEventReadonlyField() throws Exception {
		givenUserAuthorized();
		whenAddEventReadonlyField();
		thenStatusBadRequest();
		
		logoutUser();
	}
	
	// Scenario: Create event uknown field
	@Test
	public void addEventUnknownField() throws Exception {
		givenUserAuthorized();
		whenAddEventUnknownField();
		thenStatusBadRequest();
		
		logoutUser();
	}

	private void givenUserAuthorized() throws Exception {
		String email = "test@raspolozi.me";
		String password = "12345";
		mvcResult = mockMvc.perform(formLogin(APIConfig.LOGIN_URL)
									.user("email", email)
									.password("password", password))
									.andReturn();

		String jsonResult = mvcResult.getResponse().getContentAsString();
		
		if (jsonResult == null || jsonResult.isEmpty())
			throw new Exception("Could not login as user: " + email);
		
		accessToken = JsonPath.read(jsonResult, "$.access_token");
	}

	private void givenUserNotAuthenticated() {
		accessToken = null;
	}
	
	private void whenAddEvent() throws Exception {
		String eventJson = "{}";
		
		actions = mockMvc.perform(post(APIConfig.EVENTS_URL)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(eventJson));
	}
	
	private void whenAddEventSetFields() throws Exception {
		String title = "Test Event";
		String description = "EventTest created test.";
		String location = "Belgrade";
		Boolean topEvent = false;
		String owner = "coolguy@withcoolemail.com";
		int eventDate = 5487456;
		
		String eventJson = "{\r\n" + 
				"    \"title\": \"" + title + "\",\r\n" + 
				"    \"description\": \"" + description + "\",\r\n" + 
				"    \"location\": \"" + location + "\",\r\n" + 
				"    \"topEvent\": \"" + topEvent + "\",\r\n" + 
				"    \"owner\": \"" + owner + "\",\r\n" + 
				"    \"eventDate\": \"" + eventDate + "\"\r\n" + 
				"}";
		
		actions = mockMvc.perform(post(APIConfig.EVENTS_URL)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(eventJson));

	}
	
	private void whenAddEventReadonlyField() throws Exception {
		int eventId = 1;
		String eventJson = "{\r\n" + 
				"    \"id\": \"" + eventId + "\",\r\n" +
				"    \"title\": \"Title\"\r\n" + 
				"}";
		
		actions = mockMvc.perform(post(APIConfig.EVENTS_URL)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(eventJson));
	}
	
	private void whenAddEventUnknownField() throws Exception {
		String title = "Test Event";
		
		String eventJson = "{\r\n" + 
				"    \"title\": \"" + title + "\",\r\n" + 
				"    \"unknownAttribute\": \" trying to add unknown attribute\"\r\n" + 
				"}";
		
		actions = mockMvc.perform(post(APIConfig.EVENTS_URL)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(eventJson));
	}
	
	private void thenStatusCreated() throws Exception {
		actions.andExpect(status().isCreated());
	}
	
	private void thenStatusUnauthorized() throws Exception {
		actions.andExpect(status().isUnauthorized());
	}
	
	private void thenStatusBadRequest() throws Exception {
		actions.andExpect(status().isBadRequest());
	}
	
	private void thenIdGenerated() throws Exception {
		actions
			.andExpect(jsonPath("$.id").value(Matchers.notNullValue()));
	}
	
	private void thenFieldsSet() throws Exception {
		String title = "Test Event";
		String description = "EventTest created test.";
		String location = "Belgrade";
		Boolean topEvent = false;
		String owner = "coolguy@withcoolemail.com";
		int eventDate = 5487456;
		
		actions
			.andExpect(jsonPath("$.title", is(title)))
	   	   	.andExpect(jsonPath("$.description", is(description)))
	   	   	.andExpect(jsonPath("$.location", is(location)))
	   	   	.andExpect(jsonPath("$.topEvent", is(topEvent)))
	   	   	.andExpect(jsonPath("$.owner", is(owner)))
	   	   	.andExpect(jsonPath("$.eventDate", is(eventDate)))
	   	   	;
	}
	
	private void thenFieldsEmpty() throws Exception {
		actions
			.andExpect(jsonPath("$.title").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.description").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.location").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.topEvent").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.owner").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.eventDate").value(Matchers.nullValue()))
	   	   	;
	}
	
	private void deleteEventAfterAdd() throws Exception {
		MvcResult result = actions.andReturn();

		String jsonResult = result.getResponse().getContentAsString();

		ObjectMapper mapper = new ObjectMapper();
		Event testEvent = null;
		testEvent = mapper.readValue(jsonResult, Event.class);
		
		int eventId = testEvent.getId();
		
		eventRepository.delete(eventRepository.findById(eventId).orElse(null));
	}
	
	private void logoutUser() throws Exception {
		actions = mockMvc.perform(logout());
	}
}