package me.raspolozi.api.event;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.Date;

import me.raspolozi.configuration.APIConfig;
import me.raspolozi.util.TestSetup;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.jayway.jsonpath.JsonPath;

import me.raspolozi.RaspoloziMeApp;
import me.raspolozi.configuration.SecurityConfiguration;
import me.raspolozi.entities.Event;
import me.raspolozi.repository.EventRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RaspoloziMeApp.class)
@WebAppConfiguration
@ContextConfiguration(classes = SecurityConfiguration.class)
public class EventAPIPutTest {

	@Autowired
	private TestSetup testSetup;

	private static final MediaType contentType = new MediaType(
			MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), 
			Charset.forName("utf8")
			);
	
	private MockMvc mockMvc;
	private ResultActions actions;
	private MvcResult mvcResult;
	
	private String accessToken;
	
	private int eventId;
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	@Autowired
	private EventRepository eventRepository;
	
	@Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();

		testSetup.setupUsersAndRoles();
	}

	@After
	public void cleanup() throws Exception {
		eventRepository.deleteAll();

		logoutUser();

		testSetup.cleanupUsersAndRoles();
	}
	
	// Scenario: Replace event
	@Test
	public void replaceEvent() throws Exception {
		givenUserAuthorized();
		givenEventExists();
		whenReplaceEvent();
		thenStatusOK();
		thenFieldReplaced();
		
		deleteEventAfterAdd();
		logoutUser();
	}
	
	// Scenario: Replace event full
	@Test
	public void replaceEventFull() throws Exception {
		givenUserAuthorized();
		givenEventExists();
		whenReplaceEventFull();
		thenStatusOK();
		thenAllFieldsReplaced();
		
		deleteEventAfterAdd();
		logoutUser();
	}
	
	// Scenario: Replace event not authenticated
	@Test
	public void replaceEventNotAuthenticated() throws Exception {
		givenUserNotAuthenticated();
		whenReplaceEvent();
		thenStatusUnauthorized();
	}
	
	// Scenario: Replace event non existing
	@Test
	public void replaceEventNonExisting() throws Exception {
		givenUserAuthorized();
		givenEventNotExists();
		whenReplaceEvent();
		thenStatusNotFound();
		
		logoutUser();
	}
	
	// Scenario: Replace event no fields
	@Test
	public void replaceEventEmpty() throws Exception {
		givenUserAuthorized();
		givenEventExists();
		whenReplaceEventEmpty();
		thenStatusOK();
		thenAllFieldsEmpty();
		
		deleteEventAfterAdd();
		logoutUser();
	}

	// Scenario: Replace event read only field
	@Test
	public void replaceEventReadonlyField() throws Exception {
		givenUserAuthorized();
		givenEventExists();
		whenReplaceEventReadonlyField();
		thenStatusBadRequest();
		
		deleteEventAfterAdd();
		logoutUser();
	}

	// Scenario: Replace event uknown field
	@Test
	public void replaceEventUnknownField() throws Exception {
		givenUserAuthorized();
		givenEventExists();
		whenReplaceEventUnknownField();
		thenStatusBadRequest();
		
		deleteEventAfterAdd();
		logoutUser();
	}

	private void givenUserAuthorized() throws Exception {
		String email = "test@raspolozi.me";
		String password = "12345";
		mvcResult = mockMvc.perform(formLogin(APIConfig.LOGIN_URL)
									.user("email", email)
									.password("password", password))
									.andReturn();

		String jsonResult = mvcResult.getResponse().getContentAsString();
		
		if (jsonResult == null || jsonResult.isEmpty())
			throw new Exception("Could not login as user: " + email);
		
		accessToken = JsonPath.read(jsonResult, "$.access_token");
	}

	private void givenUserNotAuthenticated() {
		accessToken = null;
	}
	
	private void givenEventExists() throws Exception {
		Event newEvent = new Event();
		
		String title = "Test Event";
		String description = "EventTest created test.";
		String location = "Belgrade";
		Boolean topEvent = false;
		String owner = "coolguy@withcoolemail.com";
		int eventDate = 5487456;
		
		newEvent.setTitle(title);
		newEvent.setDescription(description);
		newEvent.setLocation(location);
		newEvent.setTopEvent(topEvent);
		newEvent.setOwner(owner);
		newEvent.setEventDate(new Date(eventDate));
		
		newEvent = eventRepository.save(newEvent);
		
		if (newEvent == null) {
			throw new Exception("Could not create new test event");
		}
		
		eventId = newEvent.getId();
	}
	
	private void givenEventNotExists() {
		eventId = -1;
	}
	
	private void whenReplaceEventFull() throws Exception {
		String title = "Event Title";
		String description = "EventTest updated.";
		String location = "Krusevac";
		Boolean topEvent = false;
		String owner = "anothercoolguy@withcoolemail.com";
		int eventDate = 123123123;
		
		String eventJson = "{\r\n" + 
				"    \"title\": \"" + title + "\",\r\n" + 
				"    \"description\": \"" + description + "\",\r\n" + 
				"    \"location\": \"" + location + "\",\r\n" + 
				"    \"topEvent\": \"" + topEvent + "\",\r\n" + 
				"    \"owner\": \"" + owner + "\",\r\n" + 
				"    \"eventDate\": \"" + eventDate + "\"\r\n" + 
				"}";
		
		actions = mockMvc.perform(put(APIConfig.EVENTS_URL + "/" + eventId)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(eventJson));
	}
	
	private void whenReplaceEvent() throws Exception {
		String title = "Test Event";
		
		String eventJson = "{\r\n" + 
				"    \"title\": \"" + title + "\"\r\n" +
				"}";
		
		actions = mockMvc.perform(put(APIConfig.EVENTS_URL + "/" + eventId)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(eventJson));
	}
	
	private void whenReplaceEventEmpty() throws Exception {
		String eventJson = "{}";
		
		actions = mockMvc.perform(put(APIConfig.EVENTS_URL + "/" + eventId)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(eventJson));
	}
	
	private void whenReplaceEventUnknownField() throws Exception {
		String eventJson = "{\r\n" + 
				"    \"UnknownAttributeKey\": \" Unknown attribute value\"\r\n" +
				"}";
		
		actions = mockMvc.perform(put(APIConfig.EVENTS_URL + "/" + eventId)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(eventJson));
	}

	private void whenReplaceEventReadonlyField() throws Exception {
		int eventId = 1;
		String eventJson = "{\r\n" + 
				"    \"id\": \"" + eventId + "\",\r\n" +
				"    \"title\": \"Event Title\"\r\n" +
				"}";
		
		actions = mockMvc.perform(put(APIConfig.EVENTS_URL + "/" + eventId)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(eventJson));
	}
	 
	private void thenStatusOK() throws Exception {
		actions.andExpect(status().isOk());
	}
	
	private void thenStatusNotFound() throws Exception {
		actions.andExpect(status().isNotFound());
	}
	
	private void thenStatusUnauthorized() throws Exception {
		actions.andExpect(status().isUnauthorized());
	}

	private void thenStatusBadRequest() throws Exception {
		actions.andExpect(status().isBadRequest());
	}
	
	private void thenFieldReplaced() throws Exception {
		String title = "Test Event";
		
		actions
			.andExpect(jsonPath("$.id").value(eventId))
			.andExpect(jsonPath("$.title", is(title)))
	   	   	.andExpect(jsonPath("$.description").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.location").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.topEvent").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.owner").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.eventDate").value(Matchers.nullValue()))
	   	   	;
	}
	
	private void thenAllFieldsReplaced() throws Exception {
		String title = "Event Title";
		String description = "EventTest updated.";
		String location = "Krusevac";
		Boolean topEvent = false;
		String owner = "anothercoolguy@withcoolemail.com";
		int eventDate = 123123123;
		
		actions
			.andExpect(jsonPath("$.id").value(eventId))
			.andExpect(jsonPath("$.title", is(title)))
	   	   	.andExpect(jsonPath("$.description", is(description)))
	   	   	.andExpect(jsonPath("$.location", is(location)))
	   	   	.andExpect(jsonPath("$.topEvent", is(topEvent)))
	   	   	.andExpect(jsonPath("$.owner", is(owner)))
	   	   	.andExpect(jsonPath("$.eventDate", is(eventDate)))
	   	   	;
	}
	
	private void thenAllFieldsEmpty() throws Exception {
		actions
			.andExpect(jsonPath("$.id").value(eventId))
			.andExpect(jsonPath("$.title").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.description").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.location").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.topEvent").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.owner").value(Matchers.nullValue()))
	   	   	.andExpect(jsonPath("$.eventDate").value(Matchers.nullValue()))
	   	   	;
	}
	
	private void deleteEventAfterAdd() {	
		eventRepository.delete(eventRepository.findById(eventId).orElse(null));
	}
	
	private void logoutUser() throws Exception {
		actions = mockMvc.perform(logout());
	}
}