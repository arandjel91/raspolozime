package me.raspolozi.api.user;

import me.raspolozi.RaspoloziMeApp;
import me.raspolozi.configuration.APIConfig;
import me.raspolozi.configuration.SecurityConfiguration;
import me.raspolozi.entities.Role;
import me.raspolozi.entities.User;
import me.raspolozi.repository.RoleRepository;
import me.raspolozi.repository.UserRepository;
import me.raspolozi.util.TestSetup;
import com.jayway.jsonpath.JsonPath;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.HashSet;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RaspoloziMeApp.class)
@WebAppConfiguration
@ContextConfiguration(classes = SecurityConfiguration.class)
public class UserAPIPatchTest {
	
	@Autowired
	private TestSetup testSetup;
	
	private static final MediaType contentType = new MediaType(
			MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), 
			Charset.forName("utf8")
			);
	
	private MockMvc mockMvc;
	private ResultActions actions;
	private MvcResult mvcResult;
	
	private int userId;
	
	private String accessToken;
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();
        
        testSetup.setupUsersAndRoles();
	}
	
	@After
	public void cleanup() throws Exception {
		userRepository.deleteAll();
		
		logoutUser();
		
        testSetup.cleanupUsersAndRoles();
	}
	
	// Scenario: Update user
	@Test
	public void updateUser() throws Exception {
		givenUserAuthorized();
		givenUserExists();
		whenUpdateUser();
		thenStatusOK();
		thenFieldUpdated();
	}
	
	// Scenario: Update user all fields
	@Test
	public void updateUserAllFields() throws Exception {	
		givenUserAuthorized();
		givenUserExists();
		whenUpdateUserAllFields();
		thenStatusOK();
		thenAllFieldsUpdated();
	}
	
	// Scenario: Update user not authenticated
	@Test
	public void updateUserNotAuthenticated() throws Exception {
		givenUserNotAuthenticated();
		whenUpdateUser();
		thenStatusUnauthorized();
	}
	
	// Scenario: Update user non existing
	@Test
	public void updateUserNonExisting() throws Exception {
		givenUserAuthorized();
		givenUserNotExists();
		whenUpdateUser();
		thenStatusNotFound();
	}
	
	// Scenario: Update user no fields
	@Test
	public void updateUserEmpty() throws Exception {
		givenUserAuthorized();
		givenUserExists();
		whenUpdateUserEmpty();
		thenStatusBadRequest();
	}
	
	// Scenario: Update user read only field
	@Test
	public void updateUserReadonlyField() throws Exception {
		givenUserAuthorized();
		givenUserExists();
		whenUpdateUserReadonlyField();
		thenStatusBadRequest();
	}

	// Scenario: Update user uknown field
	@Test
	public void updateUserUnknownField() throws Exception {
		givenUserAuthorized();
		givenUserExists();
		whenUpdateUserUnknownField();
		thenStatusBadRequest();
	}

	private void givenUserAuthorized() throws Exception {
		String email = "test@raspolozi.me";
		String password = "12345";
		mvcResult = mockMvc.perform(formLogin(APIConfig.LOGIN_URL)
									.user("email", email)
									.password("password", password))
									.andReturn();
		
		String jsonResult = mvcResult.getResponse().getContentAsString();
		
		if (jsonResult == null || jsonResult.isEmpty())
			throw new Exception("Could not login as user: " + email);
		
		accessToken = JsonPath.read(jsonResult, "$.access_token");
	}

	private void givenUserNotAuthenticated() {
		accessToken = null;
	}
	
	private void givenUserExists() throws Exception {
		HashSet<Role> roles = new HashSet<Role>();
		Role role = roleRepository.findByRole("ROLE_USER");
		roles.add(role);
		
		User newUser = new User();
		
		String email = "test.user@raspolozi.me";
		String password = "123456";
		String name = "TestUser";
		String lastName = "TestUser LastName";
		Boolean active = true;
		
		newUser.setEmail(email);
		newUser.setPassword(password);
		newUser.setName(name);
		newUser.setLastName(lastName);
		newUser.setActive(active);
		newUser.setRoles(roles);
		
		newUser = userRepository.save(newUser);
		
		if (newUser == null) {
			throw new Exception("Could not create new test user");
		}
		
		userId = newUser.getId();
	}
	
	private void givenUserNotExists() {
		userId = -1;
	}
	
	private void whenUpdateUser() throws Exception {
		String name = "UPDATENAME";
		
		String userJson = "{\r\n" + 
				"    \"name\": \"" + name + "\"\r\n" +
				"}";
		
		actions = mockMvc.perform(patch(APIConfig.USERS_URL + "/" + userId)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(userJson));
	}
	
	private void whenUpdateUserAllFields() throws Exception {
		String email = "test.user.change@raspolozi.me";
		String password = "123456";
		String name = "TestUserChange";
		String lastName = "TestUser LastNameChange";
		Boolean active = false;
		
		String userJson = "{\r\n" + 
				"    \"email\": \"" + email + "\",\r\n" + 
				"    \"password\": \"" + password + "\",\r\n" + 
				"    \"name\": \"" + name + "\",\r\n" + 
				"    \"lastName\": \"" + lastName + "\",\r\n" + 
				"    \"active\": " + active + "\r\n" +
				"}";
		
		actions = mockMvc.perform(patch(APIConfig.USERS_URL + "/" + userId)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(userJson));
	}
	
	private void whenUpdateUserEmpty() throws Exception {
		String userJson = "{}";
		
		actions = mockMvc.perform(patch(APIConfig.USERS_URL + "/" + userId)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(userJson));
	}
	
	private void whenUpdateUserUnknownField() throws Exception {
		String email = "test.user@raspolozi.me";
		String password = "123456";
		String name = "TestUser";
		String lastName = "TestUser LastName";
		Boolean active = false;
		
		String userJson = "{\r\n" + 
				"    \"unknownAttribute\": \" trying to add unknown attribute\",\r\n" + 
				"    \"email\": \"" + email + "\",\r\n" + 
				"    \"password\": \"" + password + "\",\r\n" + 
				"    \"name\": \"" + name + "\",\r\n" + 
				"    \"lastName\": \"" + lastName + "\",\r\n" + 
				"    \"active\": " + active + "\r\n" +
				"}";
		
		actions = mockMvc.perform(patch(APIConfig.USERS_URL + "/" + userId)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(userJson));
	}

	private void whenUpdateUserReadonlyField() throws Exception {
		int userId = 1;
		
		String email = "test.user.change@raspolozi.me";
		String password = "123456";
		String name = "TestUserChange";
		String lastName = "TestUser LastNameChange";
		Boolean active = false;
		
		String userJson = "{\r\n" + 
				"    \"id\": \"" + userId + "\",\r\n" +
				"    \"email\": \"" + email + "\",\r\n" + 
				"    \"password\": \"" + password + "\",\r\n" + 
				"    \"name\": \"" + name + "\",\r\n" + 
				"    \"lastName\": \"" + lastName + "\",\r\n" + 
				"    \"active\": " + active + "\r\n" +
				"}";
		
		actions = mockMvc.perform(patch(APIConfig.USERS_URL + "/" + userId)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(userJson));
	}
	 
	private void thenStatusOK() throws Exception {
		actions.andExpect(status().isOk());
	}
	
	private void thenStatusNotFound() throws Exception {
		actions.andExpect(status().isNotFound());
	}
	
	private void thenStatusUnauthorized() throws Exception {
		actions.andExpect(status().isUnauthorized());
	}

	private void thenStatusBadRequest() throws Exception {
		actions.andExpect(status().isBadRequest());
	}
	
	private void thenFieldUpdated() throws Exception {
		String email = "test.user@raspolozi.me";
		String name = "UPDATENAME";
		String lastName = "TestUser LastName";
		Boolean active = true;
		
		actions
			.andExpect(jsonPath("$.id").value(is(userId)))
			.andExpect(jsonPath("$.email").value(is(email)))
			.andExpect(jsonPath("$.name").value(is(name)))
	   	   	.andExpect(jsonPath("$.lastName").value(is(lastName)))
			.andExpect(jsonPath("$.active").value(is(active)))
	   	   	.andExpect(jsonPath("$.roles[0].role").value(is("ROLE_USER")))
	   	   	;
	}
	
	private void thenAllFieldsUpdated() throws Exception {
		String email = "test.user.change@raspolozi.me";
		String name = "TestUserChange";
		String lastName = "TestUser LastNameChange";
		Boolean active = false;
		
		actions
			.andExpect(jsonPath("$.id").value(is(userId)))
			.andExpect(jsonPath("$.email").value(is(email)))
			.andExpect(jsonPath("$.name").value(is(name)))
	   	   	.andExpect(jsonPath("$.lastName").value(is(lastName)))
			.andExpect(jsonPath("$.active").value(is(active)))
	   	   	.andExpect(jsonPath("$.roles[0].role").value(is("ROLE_USER")))
	   	   	;
	}
	
	private void logoutUser() throws Exception {
		actions = mockMvc.perform(logout());
	}
}