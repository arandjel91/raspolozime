package me.raspolozi.api.user;

import me.raspolozi.RaspoloziMeApp;
import me.raspolozi.configuration.APIConfig;
import me.raspolozi.configuration.SecurityConfiguration;
import me.raspolozi.entities.Role;
import me.raspolozi.entities.User;
import me.raspolozi.repository.RoleRepository;
import me.raspolozi.repository.UserRepository;
import me.raspolozi.util.TestSetup;
import com.jayway.jsonpath.JsonPath;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.HashSet;

import static org.junit.Assert.assertNull;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RaspoloziMeApp.class)
@WebAppConfiguration
@ContextConfiguration(classes = SecurityConfiguration.class)
public class UserAPIDeleteTest {
	
	@Autowired
	private TestSetup testSetup;
	
	private static final MediaType contentType = new MediaType(
			MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), 
			Charset.forName("utf8")
			);
	
	private MockMvc mockMvc;
	private ResultActions actions;
	private MvcResult mvcResult;
	
	private int userId;
	
	private String accessToken;
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();
        
        testSetup.setupUsersAndRoles();
	}
	
	@After
	public void cleanup() throws Exception {
		logoutUser();

		testSetup.cleanupUsersAndRoles();
	}
	
	// Scenario: Delete User
	@Test
	public void deleteUser() throws Exception {
		givenUserAuthorized();
		givenUserExists();
		whenDeleteUser();
		thenStatusOK();
		thenUserDeleted();
	}

	// Scenario: Delete User not authenticated
	@Test
	public void deleteUserNotAuthenticated() throws Exception {
		givenUserNotAuthenticated();
		whenDeleteUser();
		thenStatusUnauthorized();
	}

	// Scenario: Delete User non existing
	@Test
	public void deleteUserNonExisting() throws Exception {
		givenUserAuthorized();
		givenUserNotExists();
		whenDeleteUser();
		thenStatusNotFound();
	}

	private void givenUserAuthorized() throws Exception {
		String email = "test@raspolozi.me";
		String password = "12345";
		mvcResult = mockMvc.perform(formLogin(APIConfig.LOGIN_URL)
									.user("email", email)
									.password("password", password))
									.andReturn();
		
		String jsonResult = mvcResult.getResponse().getContentAsString();
		
		if (jsonResult == null || jsonResult.isEmpty())
			throw new Exception("Could not login as user: " + email);
		
		accessToken = JsonPath.read(jsonResult, "$.access_token");
	}

	private void givenUserNotAuthenticated() {
		accessToken = null;
	}
	
	private void givenUserExists() throws Exception {
		User newUser = new User();
		newUser.setEmail("test.user@raspolozi.me");
		newUser.setPassword("123456");
		newUser.setName("TestUser");
		newUser.setLastName("TestUser LastName");
		newUser.setActive(true);
		
		HashSet<Role> roles = new HashSet<Role>();
		Role role = roleRepository.findByRole("ROLE_USER");
		roles.add(role);
		
		newUser.setRoles(roles);
		
		newUser = userRepository.save(newUser);
		
		if (newUser == null) {
			throw new Exception("Could not create new test User");
		}
		
		userId = newUser.getId();
	}
	
	private void givenUserNotExists() {
		userId = -1;
	}
	
	private void whenDeleteUser() throws Exception {
		actions = mockMvc.perform(delete(APIConfig.USERS_URL + "/" + userId)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType));
	}
	 
	private void thenStatusOK() throws Exception {
		actions.andExpect(status().isOk());
	}
	
	private void thenStatusNotFound() throws Exception {
		actions.andExpect(status().isNotFound());
	}
	
	private void thenStatusUnauthorized() throws Exception {
		actions.andExpect(status().isUnauthorized());
	}

	private void thenUserDeleted() {
		User user = userRepository.findById(userId).orElse(null);
		assertNull("User [id=" + userId + "] has not been deleted", user);
	}
	
	private void logoutUser() throws Exception {
		actions = mockMvc.perform(logout());
	}
}