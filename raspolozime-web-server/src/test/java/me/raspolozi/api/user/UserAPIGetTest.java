package me.raspolozi.api.user;

import me.raspolozi.RaspoloziMeApp;
import me.raspolozi.configuration.APIConfig;
import me.raspolozi.configuration.SecurityConfiguration;
import me.raspolozi.entities.Role;
import me.raspolozi.entities.User;
import me.raspolozi.repository.RoleRepository;
import me.raspolozi.repository.UserRepository;
import me.raspolozi.util.TestSetup;
import com.jayway.jsonpath.JsonPath;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RaspoloziMeApp.class)
@WebAppConfiguration
@ContextConfiguration(classes = SecurityConfiguration.class)
public class UserAPIGetTest {
	
	@Autowired
	private TestSetup testSetup;
	
	private static final MediaType contentType = new MediaType(
			MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), 
			Charset.forName("utf8")
			);
	
	private MockMvc mockMvc;	
	private ResultActions actions;
	private MvcResult mvcResult;
	
	private ArrayList<Integer> existingUsersIdentifiers;
	
	private ArrayList<Integer> usersIdentifiers;
	
	private String accessToken;
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;
	
	@Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders
        		.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();
        
        testSetup.setupUsersAndRoles();
	}
	
	@After
	public void cleanup() throws Exception {
		usersIdentifiers = null;
		existingUsersIdentifiers = null;
				
		logoutUser();
		
        testSetup.cleanupUsersAndRoles();
	}
	
	// Scenario: Retrieve users
	@Test
	public void getUsers() throws Exception {
		givenUserAuthorized();
		givenUsersExists(25 - (int) userRepository.count());
		whenGetUsers();
		thenStatusOK();
		thenReceivesFirstPage();
		thenFieldsSetForEachUser(0, 25);
	}

	// Scenario: Retrieve users second page
	@Test
	public void getUsersSecondPage() throws Exception {
		givenUserAuthorized();
		givenUsersExists(50 - (int) userRepository.count());
		whenGetUsersSecondPage();
		thenStatusOK();
		thenReceivesSecondPage();
		thenFieldsSetForEachUser(1, 25);
	}

	// Scenario: Retrieve users last page
	@Test
	public void getUsersLastPage() throws Exception {
		givenUserAuthorized();
		givenUsersExists(60 - (int) userRepository.count());
		whenGetUsersLastPage();
		thenStatusOK();
		thenReceivesLastPage();
		thenFieldsSetForEachUser(2, 10);
	}
	
	// Scenario: Retrieve users 50
	@Test
	public void getUsersSize50() throws Exception {
		givenUserAuthorized();
		givenUsersExists(50 - (int) userRepository.count());
		whenGetUsersSize50();
		thenStatusOK();
		thenReceivesUsersSize50();
		thenFieldsSetForEachUser(0, 50);
	}
	
	// Scenario: Retrieve users over 100
	@Test
	public void getUsersSizeOver100() throws Exception {
		givenUserAuthorized();
		givenUsersExists(150 - (int) userRepository.count());
		whenGetUsersSizeOver100();
		thenStatusOK();
		thenReceivesUsersSize100();
		thenFieldsSetForEachUser(0, 100);
	}
	
	// Scenario: Retrieve users not authenticated
	@Test
	public void getUsersNotAuthenticated() throws Exception {
		givenUserNotAuthenticated();
		whenGetUsers();
		thenStatusUnauthorized();
	}

	// Scenario: Retrieve specific user
	@Test
	public void getUser() throws Exception {		
		givenUserAuthorized();
		givenUsersExists(1);
		whenGetUser();
		thenStatusOK();
		thenFieldsSet();
	}

	// Scenario: Retrieve specific user non existing
	@Test
	public void getUserNonExisting() throws Exception {
		givenUserAuthorized();
		givenUserNotExists();
		whenGetUser();
		thenStatusNotFound();
	}

	private void givenUserAuthorized() throws Exception {
		String email = "test@raspolozi.me";
		String password = "12345";
		mvcResult = mockMvc.perform(formLogin(APIConfig.LOGIN_URL)
									.user("email", email)
									.password("password", password))
									.andReturn();
		
		String jsonResult = mvcResult.getResponse().getContentAsString();
		
		if (jsonResult == null || jsonResult.isEmpty())
			throw new Exception("Could not login as user: " + email);
		
		accessToken = JsonPath.read(jsonResult, "$.access_token");
	}

	private void givenUserNotAuthenticated() throws Exception {
		accessToken = null;
	}

	private void givenUsersExists(int numberOfUsersForAdd) throws Exception {
		existingUsersIdentifiers = new ArrayList<Integer>();
		usersIdentifiers = new ArrayList<Integer>();
		
		for(User currentUser: userRepository.findAll()) {
			usersIdentifiers.add(currentUser.getId());
			existingUsersIdentifiers.add(currentUser.getId());
		}
		
		HashSet<Role> roles = new HashSet<Role>();
		Role role = roleRepository.findByRole("ROLE_USER");
		roles.add(role);
				
		for(int n = usersIdentifiers.size(), i = n; i < numberOfUsersForAdd + n; i++)
		{
			User testUser = new User();
			
			testUser.setEmail("test.user" + i + "@raspolozi.me");
			testUser.setPassword("123456");
			testUser.setName("TestUser");
			testUser.setLastName("TestUser LastName");
			testUser.setActive(true);
			testUser.setRoles(roles);
			
			User result = userRepository.save(testUser);
			
			if(result == null)
				throw new Exception("User wasn't added at loop number: " + i);
			
			usersIdentifiers.add(result.getId());
		}
	}
	
	private void givenUserNotExists() {
		existingUsersIdentifiers = new ArrayList<Integer>();
		usersIdentifiers = new ArrayList<Integer>();
		
		for(User currentUser: userRepository.findAll()) {
			usersIdentifiers.add(currentUser.getId());
			existingUsersIdentifiers.add(currentUser.getId());
		}
		
		usersIdentifiers.add(-1);
	}
	
	private void whenGetUsers() throws Exception {
		actions = mockMvc.perform(get(APIConfig.USERS_URL)
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType));
	}

	private void whenGetUsersSecondPage() throws Exception {
		actions = mockMvc.perform(get(APIConfig.USERS_URL + "?page=1")
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType));
	}

	private void whenGetUsersLastPage() throws Exception {
		actions = mockMvc.perform(get(APIConfig.USERS_URL + "?page=2")
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType));
	}
	
	private void whenGetUsersSize50() throws Exception {
		actions = mockMvc.perform(get(APIConfig.USERS_URL + "?size=50")
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType));
	}
	
	private void whenGetUsersSizeOver100() throws Exception {
		actions = mockMvc.perform(get(APIConfig.USERS_URL + "?size=120")
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType));
	}
	
	private void whenGetUser() throws Exception {
		int userId = usersIdentifiers.get(existingUsersIdentifiers.size());
		actions = mockMvc.perform(get(APIConfig.USERS_URL + "/" + userId)
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType));
	}
	
	private void thenStatusOK() throws Exception {
		actions.andExpect(status().isOk());
	}
	
	private void thenStatusNotFound() throws Exception {
		actions.andExpect(status().isNotFound());
	}
	
	private void thenStatusUnauthorized() throws Exception {
		actions.andExpect(status().isUnauthorized());
	}
	
	private void thenReceivesFirstPage() throws Exception {
		actions
			.andExpect(jsonPath("$.first", is(true)))
			.andExpect(jsonPath("$.number", is(0)))
			.andExpect(jsonPath("$.size", is(25)))
			.andExpect(jsonPath("$.numberOfElements", is(25)))
			.andExpect(jsonPath("$.totalElements", is(25)));
	}
	
	private void thenReceivesSecondPage() throws Exception {
		actions
			.andExpect(jsonPath("$.first", is(false)))
			.andExpect(jsonPath("$.number", is(1)))
			.andExpect(jsonPath("$.size", is(25)))
			.andExpect(jsonPath("$.numberOfElements", is(25)))
			.andExpect(jsonPath("$.totalElements", is(50)));
	}
	
	private void thenReceivesLastPage() throws Exception {
		actions
		.andExpect(jsonPath("$.last", is(true)))
		.andExpect(jsonPath("$.number", is(2)))
		.andExpect(jsonPath("$.size", is(25)))
		.andExpect(jsonPath("$.numberOfElements", is(10)))
		.andExpect(jsonPath("$.totalElements", is(60)));
	}
	
	private void thenReceivesUsersSize50() throws Exception {
		actions
		.andExpect(jsonPath("$.first", is(true)))
		.andExpect(jsonPath("$.number", is(0)))
		.andExpect(jsonPath("$.size", is(50)))
		.andExpect(jsonPath("$.numberOfElements", is(50)))
		.andExpect(jsonPath("$.totalElements", is(50)));
	}
	
	private void thenReceivesUsersSize100() throws Exception {
		actions
		.andExpect(jsonPath("$.first", is(true)))
		.andExpect(jsonPath("$.number", is(0)))
		.andExpect(jsonPath("$.size", is(100)))
		.andExpect(jsonPath("$.numberOfElements", is(100)))
		.andExpect(jsonPath("$.totalElements", is(150)));
	}
	
	private void thenFieldsSetForEachUser(int page, int numberOfPageElements) throws Exception {
		for(int i = 0, iterator = i+page*25; i < numberOfPageElements; i++, iterator = i+page*25) {
			
			int userId = usersIdentifiers.get(iterator);

			if(existingUsersIdentifiers.contains(userId))
				continue;
			
			actions
				.andExpect(jsonPath("$.content[" + i + "].id").value(is(userId)))
				.andExpect(jsonPath("$.content[" + i + "].email").value(is("test.user" + iterator + "@raspolozi.me")))
				.andExpect(jsonPath("$.content[" + i + "].name").value(is("TestUser")))
		   	   	.andExpect(jsonPath("$.content[" + i + "].lastName").value(is("TestUser LastName")))
				.andExpect(jsonPath("$.content[" + i + "].active").value(is(true)))
		   	   	.andExpect(jsonPath("$.content[" + i + "].roles[0].role").value(is("ROLE_USER")))
		   	   	;
		}
	}
	
	private void thenFieldsSet() throws Exception {
		int userId = usersIdentifiers.get(existingUsersIdentifiers.size());

		actions
			.andExpect(jsonPath("$.id").value(is(userId)))
			.andExpect(jsonPath("$.email").value(is("test.user" + existingUsersIdentifiers.size() + "@raspolozi.me")))
			.andExpect(jsonPath("$.name").value(is("TestUser")))
	   	   	.andExpect(jsonPath("$.lastName").value(is("TestUser LastName")))
			.andExpect(jsonPath("$.active").value(is(true)))
	   	   	.andExpect(jsonPath("$.roles[0].role").value(is("ROLE_USER")))
	   	   	;
	}
	
	private void logoutUser() throws Exception {
		actions = mockMvc.perform(logout());
	}
}