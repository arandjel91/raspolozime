package me.raspolozi.api.user;

import me.raspolozi.RaspoloziMeApp;
import me.raspolozi.configuration.APIConfig;
import me.raspolozi.configuration.SecurityConfiguration;
import me.raspolozi.util.TestSetup;
import com.jayway.jsonpath.JsonPath;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RaspoloziMeApp.class)
@WebAppConfiguration
@ContextConfiguration(classes = SecurityConfiguration.class)
public class UserAPIPostTest {
	
	@Autowired
	private TestSetup testSetup;
	
	private static final MediaType contentType = new MediaType(
			MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), 
			Charset.forName("utf8")
			);
	
	private MockMvc mockMvc;
	private ResultActions actions;
	private MvcResult mvcResult;
	
	private String accessToken;
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	@Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();
        
        testSetup.setupUsersAndRoles();
	}
	
	@After
	public void cleanup() throws Exception {
		logoutUser();
		
        testSetup.cleanupUsersAndRoles();
	}
	
	// Scenario: Create user
	@Test
	public void addUserEmpty() throws Exception {
		givenUserAuthorized();
		whenAddUserEmpty();
		thenStatusBadRequest();
	}
	
	// Scenario: Create user set fields
	@Test
	public void addUserSetFields() throws Exception {
		givenUserAuthorized();
		whenAddUserSetFields();
		thenStatusCreated();
		thenIdGenerated();
		thenFieldsSet();
	}

	// Scenario: Create user not authenticated
	@Test
	public void addUserNotAuthenticated() throws Exception {	
		givenUserNotAuthenticated();
		whenAddUserEmpty();
		thenStatusUnauthorized();
	}
	
	// Scenario: Create user read only field
	@Test
	public void addUserReadonlyField() throws Exception {
		givenUserAuthorized();
		whenAddUserReadonlyField();
		thenStatusBadRequest();
	}
	
	// Scenario: Create user unknown field
	@Test
	public void addUserUnknownField() throws Exception {
		givenUserAuthorized();
		whenAddUserUnknownField();
		thenStatusBadRequest();
	}

	private void givenUserAuthorized() throws Exception {
		String email = "test@raspolozi.me";
		String password = "12345";
		mvcResult = mockMvc.perform(formLogin(APIConfig.LOGIN_URL)
									.user("email", email)
									.password("password", password))
									.andReturn();
		
		String jsonResult = mvcResult.getResponse().getContentAsString();
		
		if (jsonResult == null || jsonResult.isEmpty())
			throw new Exception("Could not login as user: " + email);
		
		accessToken = JsonPath.read(jsonResult, "$.access_token");
	}

	private void givenUserNotAuthenticated() {
		accessToken = null;
	}
	
	private void whenAddUserEmpty() throws Exception {
		String userJson = "{}";
		
		actions = mockMvc.perform(post(APIConfig.USERS_URL)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(userJson));
	}
	
	private void whenAddUserSetFields() throws Exception {
		String email = "test.user@raspolozi.me";
		String password = "123456";
		String name = "TestUser";
		String lastName = "TestUser LastName";
		Boolean active = false;
		
		String userJson = "{\r\n" + 
				"    \"email\": \"" + email + "\",\r\n" + 
				"    \"password\": \"" + password + "\",\r\n" + 
				"    \"name\": \"" + name + "\",\r\n" + 
				"    \"lastName\": \"" + lastName + "\",\r\n" + 
				"    \"active\": " + active + "\r\n" +
				"}";
		
		actions = mockMvc.perform(post(APIConfig.USERS_URL)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(userJson));

	}
	
	private void whenAddUserReadonlyField() throws Exception {
		int userId = 1;

		String email = "test.user@raspolozi.me";
		String password = "123456";
		String name = "TestUser";
		String lastName = "TestUser LastName";
		Boolean active = false;
		
		String userJson = "{\r\n" + 
				"    \"id\": \"" + userId + "\",\r\n" +
				"    \"email\": \"" + email + "\",\r\n" + 
				"    \"password\": \"" + password + "\",\r\n" + 
				"    \"name\": \"" + name + "\",\r\n" + 
				"    \"lastName\": \"" + lastName + "\",\r\n" + 
				"    \"active\": " + active + "\r\n" +
				"}";
		
		actions = mockMvc.perform(post(APIConfig.USERS_URL)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(userJson));
	}
	
	private void whenAddUserUnknownField() throws Exception {
		String email = "test.user@raspolozi.me";
		String password = "123456";
		String name = "TestUser";
		String lastName = "TestUser LastName";
		Boolean active = false;
		
		String userJson = "{\r\n" + 
				"    \"unknownAttribute\": \" trying to add unknown attribute\",\r\n" + 
				"    \"email\": \"" + email + "\",\r\n" + 
				"    \"password\": \"" + password + "\",\r\n" + 
				"    \"name\": \"" + name + "\",\r\n" + 
				"    \"lastName\": \"" + lastName + "\",\r\n" + 
				"    \"active\": " + active + "\r\n" +
				"}";
		
		actions = mockMvc.perform(post(APIConfig.USERS_URL)
									.with(csrf())
									.header("Authorization", "Bearer " + accessToken)
									.contentType(contentType)
									.content(userJson));
	}
	
	private void thenStatusCreated() throws Exception {
		actions.andExpect(status().isCreated());
	}
	
	private void thenStatusUnauthorized() throws Exception {
		actions.andExpect(status().isUnauthorized());
	}
	
	private void thenStatusBadRequest() throws Exception {
		actions.andExpect(status().isBadRequest());
	}
	
	private void thenIdGenerated() throws Exception {
		actions
			.andExpect(jsonPath("$.id").value(Matchers.notNullValue()));
	}
	
	private void thenFieldsSet() throws Exception {
		String email = "test.user@raspolozi.me";
		String name = "TestUser";
		String lastName = "TestUser LastName";
		Boolean active = false;
		
		actions
			.andExpect(jsonPath("$.email").value(is(email)))
			.andExpect(jsonPath("$.name").value(is(name)))
	   	   	.andExpect(jsonPath("$.lastName").value(is(lastName)))
			.andExpect(jsonPath("$.active").value(is(active)))
	   	   	.andExpect(jsonPath("$.roles[0].role").value(is("ROLE_USER")))
	   	   	;
	}
	
	private void logoutUser() throws Exception {
		actions = mockMvc.perform(logout());
	}
}