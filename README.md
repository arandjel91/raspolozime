# Oraspolozi me

Web app to help people organise public charity or youth work action type events.

This project implements stateless security with JWT, server written in Spring Boot, and frontend written in Angular4+.  
(Forsaken) Branch *stateful-security-with-mvc-and-api* contains earlier implementation with stateful security and MVC with API, written in Spring Boot and Thymeleaf.

## Getting Started && Development

Add git repository to your local setup.  
Create new branch from 'dev' branch, and do your development on it. Push that branch to remote.  
When job is complete 'admin' will merge your branch to 'dev' and later to 'master', after that your branch shall be purged to void.

### Prerequisites

You are going to need some IDE for development, and local MySQL database setup with matching access from existing project (found in context file).

```
Spring tool suite
Local MySQL database
```

### Installing

Internet guides.

## Running the tests

As any other JUnit test.

## Deployment

Spring tool suite comes with Pivotal Server which can be use for quick deployment.

## Built With

* [Spring Boot](https://spring.io) - The web framework used for building web server
* [Maven](https://maven.apache.org) - Dependency Management
* [Hibernate]
* [JPA]
* [JWT](https://jwt.io) - Security for web server
* [Angular](https://angular.io) - Frontend web client

## Versioning

* [Procatinator] (https://procatinator.com/)

## Authors

* **Milos Randjelovic** - *Just getting started* - [linkedin](https://rs.linkedin.com/in/milosrandjelovic)

## License

Work on black. [LICENSE.md](LICENSE.md)

## Acknowledgments

* Thank you Nikola Tesla for giving us electricity!